﻿using LendFoundry.Applications.Filters.Api.Controllers;
using LendFoundry.Applications.Filters.Common.Tests;
using LendFoundry.Applications.Filters.Common.Tests.Fakes;
using LendFoundry.AssignmentEngine;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using Microsoft.AspNet.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace LendFoundry.Applications.Filters.Api.Tests
{
    public class ApplicationFilterControllerTests : InMemoryObjects
    {
        [Fact]
        public async void GetTotalSubmittedForCurrentMonthExecutionOk()
        {
            // arrange
            var repo = new FakeRepository();
            var sourceType = InMemoryFilterView.SourceType;
            var sourceId = InMemoryFilterView.SourceId;
            var entries = new[]
            {
                InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                InMemoryFilterView
            };
            entries.Select((entry, i) =>
            {
                i++;
                entry.Id = $"ApplicationId{i}";
                if (i > 3)
                    entry.Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.AddDays(1).Day);
                else
                    entry.Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.AddDays(-31).Month, DateTime.Now.AddDays(-10).Day);

                return entry;
            }).ToList().ForEach((filterView) =>
            {
                repo.Add(filterView);
            });

            TenantTime.Setup(x => x.TimeZone)
                .Returns(TimeZoneInfo.Utc);

            TenantTime.Setup(x => x.Now)
                .Returns(DateTime.Now);

            // action
            var controller = GetController(GetService(repo, null, null, TenantTime.Object));

            var result = await controller.GetTotalSubmittedForCurrentMonth(sourceType, sourceId);

            // assert
            Assert.Equal(7, ((HttpOkObjectResult)result).Value);
            Assert.Equal(200, ((HttpOkObjectResult)result).StatusCode);
        }

        [Fact]
        public async void GetTotalSubmittedForCurrentMonthExecutionOkButIfNoData()
        {
            // arrange
            var repo = new FakeRepository();
            var sourceType = InMemoryFilterView.SourceType;
            var sourceId = InMemoryFilterView.SourceId;
            var entries = new[]
            {
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView
                };
            entries.Select((entry, i) =>
            {
                i++;
                entry.Id = $"ApplicationId{i}";
                entry.Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.AddDays(-31).Month, DateTime.Now.AddDays(-10).Day);

                return entry;
            }).ToList().ForEach((filterView) =>
            {
                repo.Add(filterView);
            });

            Logger.Setup(x => x.Error(It.IsAny<string>()));

            TenantTime.Setup(x => x.TimeZone)
                .Returns(TimeZoneInfo.Utc);

            TenantTime.Setup(x => x.Now)
                .Returns(DateTime.Now);

            // action
            var controller = GetController(GetService(repo, null, null, TenantTime.Object));

            var result = await controller.GetTotalSubmittedForCurrentMonth(sourceType, sourceId);

            // assert
            Assert.Equal(0, ((HttpOkObjectResult)result).Value);
            Assert.Equal(200, ((HttpOkObjectResult)result).StatusCode);
        }

        [Fact]
        public async void GetTotalSubmittedForCurrentMonthWhenInvalidSourceType()
        {
            var controller = GetController(GetService());
            var result = await controller.GetTotalSubmittedForCurrentMonth(null, "SourceId");

            // assert
            Assert.IsType<ErrorResult>(result);
            Assert.IsType<FieldError>(((ErrorResult)result).Value);
            Assert.Equal(400, ((ErrorResult)result).StatusCode);
        }

        [Fact]
        public async void GetTotalSubmittedForCurrentMonthWhenInvalidSourceId()
        {
            var controller = GetController(GetService());
            var result = await controller.GetTotalSubmittedForCurrentMonth("SourceType", string.Empty);

            // assert
            Assert.IsType<ErrorResult>(result);
            Assert.IsType<FieldError>(((ErrorResult)result).Value);
            Assert.Equal(400, ((ErrorResult)result).StatusCode);
        }

        [Fact]
        public async void GetTotalSubmittedForCurrentYearExecutionOk()
        {
            // arrange
            FakeConfiguration.SubmittedStatuses = new string[] { "400.03", "500.02", "500.03", "500.05", "500.06", "600.01", "600.02", "600.03", "600.04" };

            var repo = new FakeRepository();
            
            var sourceType = InMemoryFilterView.SourceType;
            var sourceId = InMemoryFilterView.SourceId;
            var entries = new[]
            {
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView
                };
            entries.Select((entry, i) =>
            {
                i++;
                entry.Id = $"ApplicationId{i}";
                if (i > 5)
                    entry.Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.AddDays(1).Day);
                else
                    entry.Submitted = new DateTime(DateTime.Now.AddYears(-1).Year, DateTime.Now.Month, DateTime.Now.Day);

                return entry;
            }).ToList().ForEach((filterView) =>
            {
                repo.Add(filterView);
            });

            Logger.Setup(x => x.Error(It.IsAny<string>()));

            TenantTime.Setup(x => x.TimeZone)
                .Returns(TimeZoneInfo.Utc);

            TenantTime.Setup(x => x.Now)
                .Returns(DateTime.Now);

            // action
            var controller = GetController(GetService(repo, null, Logger.Object, TenantTime.Object, FakeConfiguration));

            var result = await controller.GetTotalSubmittedForCurrentYear(sourceType, sourceId);

            // assert
            Assert.Equal(0, ((HttpOkObjectResult)result).Value);
            Assert.Equal(200, ((HttpOkObjectResult)result).StatusCode);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.Never);
            TenantTime.Verify(x => x.Now, Times.AtLeastOnce);
        }

        [Fact]
        public async void GetTotalSubmittedForCurrentYearExecutionOkButIfNoData()
        {
            // arrange
            var repo = new FakeRepository();
            var sourceType = InMemoryFilterView.SourceType;
            var sourceId = InMemoryFilterView.SourceId;
            var entries = new[]
            {
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView
                };
            entries.Select((entry, i) =>
            {
                i++;
                entry.Id = $"ApplicationId{i}";
                entry.Submitted = new DateTime(DateTime.Now.AddYears(-1).Year, DateTime.Now.Month, DateTime.Now.Day);

                return entry;
            }).ToList().ForEach((filterView) =>
            {
                repo.Add(filterView);
            });

            Logger.Setup(x => x.Error(It.IsAny<string>()));

            TenantTime.Setup(x => x.TimeZone)
                .Returns(TimeZoneInfo.Utc);

            TenantTime.Setup(x => x.Now)
                .Returns(DateTime.Now);

            // action
            var controller = GetController(GetService(repo, null, Logger.Object, TenantTime.Object));

            var result = await controller.GetTotalSubmittedForCurrentYear(sourceType, sourceId);

            // assert
            Assert.Equal(0, ((HttpOkObjectResult)result).Value);
            Assert.Equal(200, ((HttpOkObjectResult)result).StatusCode);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.Never);
            TenantTime.Verify(x => x.Now, Times.AtLeastOnce);
        }

        [Fact]
        public async void GetTotalSubmittedForCurrentYearWhenInvalidSourceType()
        {
            var controller = GetController(GetService());
            var result = await controller.GetTotalSubmittedForCurrentYear(null, "SourceId");

            // assert
            Assert.IsType<ErrorResult>(result);
            Assert.IsType<FieldError>(((ErrorResult)result).Value);
            Assert.Equal(400, ((ErrorResult)result).StatusCode);
        }

        [Fact]
        public async void GetTotalSubmittedForCurrentYearWhenInvalidSourceId()
        {
            var controller = GetController(GetService());
            var result = await controller.GetTotalSubmittedForCurrentYear("SourceType", string.Empty);

            // assert
            Assert.IsType<ErrorResult>(result);
            Assert.IsType<FieldError>(((ErrorResult)result).Value);
            Assert.Equal(400, ((ErrorResult)result).StatusCode);
        }

        [Fact]
        public async void GetByName()
        {
            var fakeRepository = new FakeRepository();

            var sourceType = InMemoryFilterView.SourceType;
            var sourceId = InMemoryFilterView.SourceId;
            var name = InMemoryFilterView.Applicant;

            var entries = new[]
           {
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView
                };
            entries.Select((entry, i) =>
            {
                i++;
                entry.Id = $"ApplicationId{i}";
                entry.Applicant = "test";

                return entry;
            }).ToList().ForEach((filterView) =>
            {
                fakeRepository.Add(filterView);
            });

            Logger.Setup(x => x.Error(It.IsAny<string>()));

            TenantTime.Setup(x => x.TimeZone)
                .Returns(TimeZoneInfo.Utc);

            TenantTime.Setup(x => x.Now)
                .Returns(DateTime.Now);

            var controller = GetController(GetService(fakeRepository, null, Logger.Object, TenantTime.Object));
            var result = await controller.GetByName(sourceType, sourceId, name);

            Assert.Equal(200, ((HttpOkObjectResult)result).StatusCode);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.Never);
        }

        [Fact]
        public void GetAllBySource()
        {
            var fakeRepository = new FakeRepository();

            var sourceType = InMemoryFilterView.SourceType;
            var sourceId = InMemoryFilterView.SourceId;

            var entries = new[]
            {
                InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                InMemoryFilterView, InMemoryFilterView, InMemoryFilterView
            };
            entries.Select((entry, i) =>
            {
                i++;
                entry.Id = $"ApplicationId{i}";
                entry.Applicant = "test";

                return entry;
            }).ToList().ForEach(filterView => { fakeRepository.Add(filterView); });

            Logger.Setup(x => x.Error(It.IsAny<string>()));

            TenantTime.Setup(x => x.TimeZone)
                .Returns(TimeZoneInfo.Utc);

            TenantTime.Setup(x => x.Now)
                .Returns(DateTime.Now);

            var controller = GetController(GetService());

            var result = controller.GetAllBySource(sourceType, sourceId);

            Assert.Equal(200, ((HttpOkObjectResult)result).StatusCode);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.Never);
        }

        [Fact]
        public async void GetAllByStatus()
        {
            var fakeRepository = new FakeRepository();

            var sourceType = InMemoryFilterView.SourceType;
            var sourceId = InMemoryFilterView.SourceId;

            var inputList = new[] {
                            InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                            InMemoryFilterView, InMemoryFilterView, InMemoryFilterView };
            inputList.Select((item, idx) =>
            {
                idx++;
                item.Id = $"ApplicationId1{idx}";
                item.Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                item.StatusCode = "100.01";
                return item;
            }).ToList().ForEach(item => fakeRepository.Add(item));

            inputList.Select((item, idx) =>
            {
                idx++;
                item.Id = $"ApplicationId2{idx}";
                item.Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                item.StatusCode = "100.02";
                return item;
            }).ToList().ForEach(item => fakeRepository.Add(item));
            Logger.Setup(x => x.Error(It.IsAny<string>()));

            TenantTime.Setup(x => x.TimeZone)
                .Returns(TimeZoneInfo.Utc);

            TenantTime.Setup(x => x.Now)
                .Returns(DateTime.Now);

            var controller = GetController(GetService(fakeRepository, null, Logger.Object, TenantTime.Object));

            var result = await controller.GetByStatus(sourceType, sourceId, "100.01");

            Assert.Equal(200, ((HttpOkObjectResult)result).StatusCode);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.Never);
        }

        [Fact]
        public async void GetCountByStatus()
        {
            // arrange
            var repo = new FakeRepository();
            var sourceType = InMemoryFilterView.SourceType;
            var sourceId = InMemoryFilterView.SourceId;
            var entries = new[]
            {
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView
                };
            entries.Select((entry, i) =>
            {
                i++;
                entry.Id = $"ApplicationId{i}";
                entry.Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                entry.StatusCode = "100.01";
                return entry;
            }).ToList().ForEach((filterView) =>
            {
                repo.Add(filterView);
            });

            Logger.Setup(x => x.Error(It.IsAny<string>()));

            TenantTime.Setup(x => x.TimeZone)
                .Returns(TimeZoneInfo.Utc);

            TenantTime.Setup(x => x.Now)
                .Returns(DateTime.Now);

            var controller = GetController(GetService(repo, null, null, TenantTime.Object));
            var result = await controller.GetCountByStatus(sourceType, sourceId, "100.01");

            Assert.Equal(entries.Count(), ((HttpOkObjectResult)result).Value);
            Assert.Equal(200, ((HttpOkObjectResult)result).StatusCode);
        }

        [Fact]
        public async void GetByTag()
        {
            var fakeRepository = new FakeRepository();

            var sourceType = InMemoryFilterView.SourceType;
            var sourceId = InMemoryFilterView.SourceId;

            var inputList = new[] {
                            InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                            InMemoryFilterView, InMemoryFilterView, InMemoryFilterView };
            inputList.Select((item, idx) =>
            {
                idx++;
                item.Id = $"ApplicationId1{idx}";
                item.Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                item.Tags = new[] { "tag-a", "tag-b" };
                return item;
            }).ToList().ForEach(item => fakeRepository.Add(item));

            inputList.Select((item, idx) =>
            {
                idx++;
                item.Id = $"ApplicationId2{idx}";
                item.Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                item.Tags = new[] { "tag-c", "tag-d" };
                return item;
            }).ToList().ForEach(item => fakeRepository.Add(item));
            Logger.Setup(x => x.Error(It.IsAny<string>()));

            TenantTime.Setup(x => x.TimeZone)
                .Returns(TimeZoneInfo.Utc);

            TenantTime.Setup(x => x.Now)
                .Returns(DateTime.Now);

            var controller = GetController(GetService(fakeRepository, null, Logger.Object, TenantTime.Object));

            var result = await controller.GetByTag(sourceType, sourceId, "tag-a");

            Assert.Equal(200, ((HttpOkObjectResult)result).StatusCode);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.Never);
        }

        [Fact]
        public async void GetCountByTag()
        {
            // arrange
            var repo = new FakeRepository();
            var sourceType = InMemoryFilterView.SourceType;
            var sourceId = InMemoryFilterView.SourceId;
            var entries = new[]
            {
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView
                };
            entries.Take(2).Select((entry, i) =>
            {
                i++;
                entry.Id = $"ApplicationId{i}";
                entry.Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                entry.Tags = new[] { "tag-a", "tag-b" };
                return entry;
            }).ToList().ForEach((filterView) =>
            {
                repo.Add(filterView);
            });

            Logger.Setup(x => x.Error(It.IsAny<string>()));

            TenantTime.Setup(x => x.TimeZone)
                .Returns(TimeZoneInfo.Utc);

            TenantTime.Setup(x => x.Now)
                .Returns(DateTime.Now);

            var controller = GetController(GetService(repo, null, null, TenantTime.Object));
            var result = await controller.GetCountByTag(sourceType, sourceId, "tag-b");

            Assert.Equal(2, ((HttpOkObjectResult)result).Value);
            Assert.Equal(200, ((HttpOkObjectResult)result).StatusCode);
        }

        [Fact]
        public async void GetByApplicationNumberExecutionOk()
        {
            // arrange
            var repo = new FakeRepository();
            var sourceType = InMemoryFilterView.SourceType;
            var sourceId = InMemoryFilterView.SourceId;
            var entries = new[]
            {
                InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                InMemoryFilterView
            };
            entries.Select((entry, i) =>
            {
                i++;
                entry.Id = $"ApplicationId{i}";
                entry.ApplicationId = $"ApplicationId{i}";
                entry.Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.AddDays(1).Day);

                return entry;
            }).ToList().ForEach((filterView) =>
            {
                repo.Add(filterView);
            });

            TenantTime.Setup(x => x.TimeZone)
                .Returns(TimeZoneInfo.Utc);

            TenantTime.Setup(x => x.Now)
                .Returns(DateTime.Now);

            // action
            var controller = GetController(GetService(repo, null, null, TenantTime.Object));

            var result = await controller.GetByApplicationNumber(sourceType, sourceId, "ApplicationId2");

            // assert
            Assert.Equal("ApplicationId2", ((IFilterView)((HttpOkObjectResult)result).Value).ApplicationId);
            Assert.Equal(200, ((HttpOkObjectResult)result).StatusCode);
        }

        [Fact]
        public void GetByApplicationNumberExecutionOkButIfNoData()
        {
            // arrange
            var repo = new FakeRepository();
            var sourceType = InMemoryFilterView.SourceType;
            var sourceId = InMemoryFilterView.SourceId;
            var entries = new[]
            {
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                    InMemoryFilterView
                };
            entries.Select((entry, i) =>
            {
                i++;
                entry.Id = $"ApplicationId{i}";
                entry.ApplicationId = $"ApplicationId{i}";
                entry.Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.AddDays(1).Day);

                return entry;
            }).ToList().ForEach((filterView) =>
            {
                repo.Add(filterView);
            });

            Logger.Setup(x => x.Error(It.IsAny<string>()));

            TenantTime.Setup(x => x.TimeZone)
                .Returns(TimeZoneInfo.Utc);

            TenantTime.Setup(x => x.Now)
                .Returns(DateTime.Now);

            // action
            var controller = GetController(GetService(repo, null, null, TenantTime.Object));

            var result = controller.GetByApplicationNumber(sourceType, sourceId, "ApplicationNumber").Result;

            // assert
            Assert.IsType<ErrorResult>(result);
            Assert.Equal(404, ((ErrorResult)result).StatusCode);
        }

        [Fact]
        public async void GetByApplicationNumberWhenInvalidSourceType()
        {
            var controller = GetController(GetService());
            var result = await controller.GetByApplicationNumber(null, "SourceId", "ApplicationNumber");

            // assert
            Assert.IsType<ErrorResult>(result);
            Assert.IsType<FieldError>(((ErrorResult)result).Value);
            Assert.Equal(400, ((ErrorResult)result).StatusCode);
        }

        [Fact]
        public async void GetByApplicationNumberWhenInvalidSourceId()
        {
            var controller = GetController(GetService());
            var result = await controller.GetByApplicationNumber("SourceType", string.Empty, "ApplicationNumber");

            // assert
            Assert.IsType<ErrorResult>(result);
            Assert.IsType<FieldError>(((ErrorResult)result).Value);
            Assert.Equal(400, ((ErrorResult)result).StatusCode);
        }

        [Fact]
        public async void GetByApplicationNumberWhenInvalidApplicationNumber()
        {
            var controller = GetController(GetService());
            var result = await controller.GetByApplicationNumber("SourceType", "SourceId", string.Empty);

            // assert
            Assert.IsType<ErrorResult>(result);
            Assert.IsType<FieldError>(((ErrorResult)result).Value);
            Assert.Equal(400, ((ErrorResult)result).StatusCode);
        }

        [Fact]
        public async void GetAllExpiredApplications()
        {
            TenantTime.Setup(x => x.TimeZone)
                .Returns(TimeZoneInfo.Utc);

            TenantTime.Setup(x => x.Now)
                .Returns(DateTime.Now);
          
            FakeConfiguration.StatusesNotExpired = new string[] { "400.03", "500.02", "500.03", "500.05", "500.06", "600.01", "600.02", "600.03", "600.04" };
            var fakeRepository = new FakeRepository(TenantTime.Object);

            var sourceType = InMemoryFilterView.SourceType;
            var sourceId = InMemoryFilterView.SourceId;

            var inputList = new[] {
                            InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                            InMemoryFilterView, InMemoryFilterView, InMemoryFilterView };
            inputList.Select((item, idx) =>
            {
                idx++;
                item.Id = $"ApplicationId1{idx}";
                item.Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                item.ExpirationDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).AddDays(30);
                item.StatusCode = "100.01";
                return item;
            }).ToList().ForEach(item => fakeRepository.Add(item));

            inputList.Select((item, idx) =>
            {
                idx++;
                item.Id = $"ApplicationId2{idx}";
                item.Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                item.ExpirationDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).AddDays(30);
                item.StatusCode = "100.02";
                return item;
            }).ToList().ForEach(item => fakeRepository.Add(item));
            Logger.Setup(x => x.Error(It.IsAny<string>()));

            

            var controller = GetController(GetService(fakeRepository, null, Logger.Object, TenantTime.Object,FakeConfiguration));

            var result = await controller.GetAllExpiredApplications();

            Assert.Equal(200, ((HttpOkObjectResult)result).StatusCode);
            Logger.Verify(x => x.Error(It.IsAny<string>()), Times.Never);
        }

        [Fact]
        public async void GetUnassignedByStatus()
        {
            // arrange
            var fakeRepository = new FakeRepository();
            var filterViews = new[]
            {
                InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                InMemoryFilterView, InMemoryFilterView, InMemoryFilterView
            };

            filterViews.Select((item, idx) =>
            {
                idx++;
                item.Id = $"ApplicationId1{idx}";
                item.Applicant = $"Applicant00{idx}";
                item.Ssn = "ABCD1234";
                item.Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                item.ExpirationDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).AddDays(30);
                item.StatusCode = "100.01";
                item.Assignees = new[] {
                        new Assignment
                        {
                            Id = "1",
                            AssignedBy = "Eduardo",
                            AssignedOn = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day),
                            Assignee = "Eduardo",
                            EntityId = "EntityId",
                            EntityType = "Application",
                            EscalationId = "EscalationId",
                            Role = "Credit Specialist",
                            TenantId = TenantId
                        }
                    }.ToDictionary(x => x.Role, x => x.Assignee);
                return item;
            }).ToList().ForEach(item => fakeRepository.Add(item));

            filterViews.Select((item, idx) =>
            {
                idx++;
                return new FilterView
                {
                    Id = $"ApplicationId2{idx}",
                    Applicant = $"Applicant00{idx}",
                    Ssn = "ABCD1234",
                    Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day),
                    ExpirationDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).AddDays(30),
                    StatusCode = "100.02",
                    Amount = 12000,
                    MaxApproval = 10000,
                    PaymentAmount = 5000,
                    Payout = 5000,
                    SourceId = "SourceId1",
                    SourceType = "SourceType1",
                    Assignees = new[] {
                        new Assignment
                        {
                            Id = "1",
                            AssignedBy = "Eduardo",
                            AssignedOn = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day),
                            Assignee = "Eduardo",
                            EntityId = "EntityId",
                            EntityType = "Application",
                            EscalationId = "EscalationId",
                            Role = "Credit Specialist",
                            TenantId = TenantId
                        }
                    }.ToDictionary(x => x.Role, x => x.Assignee)
                };
            }).ToList().ForEach(item => fakeRepository.Add(item));

            TokenHandler.Setup(x => x.Parse(It.IsAny<string>()))
                .Returns(InMemoryToken);

            var roles = new[] { "QA", "VP of Sales" };
            IdentityService.Setup(x => x.GetUserRoles(It.IsAny<string>()))
                .ReturnsAsync(roles);

            // action
            var controller = GetController(GetService(fakeRepository, null, null, null, null, null, TokenHandler.Object, IdentityService.Object));
            var result = await controller.GetUnassignedByStatus("100.01/100.02");

            // assert
            Assert.Equal(200, ((HttpOkObjectResult)result).StatusCode);
            Assert.NotEmpty(((IEnumerable<IFilterViewUnassigned>)((ObjectResult)result).Value));
            Assert.Equal(filterViews.Count() * 2, ((IEnumerable<IFilterViewUnassigned>)((ObjectResult)result).Value).Count());
            Assert.True(((IEnumerable<IFilterViewUnassigned>)((ObjectResult)result).Value).All(u => u.Last4Ssn == "1234"));
            Assert.True(((IEnumerable<IFilterViewUnassigned>)((ObjectResult)result).Value).All(u => u.ApplicationDate.Date == DateTime.Now.Date));
        }

        [Fact]
        public async void GetUnassignedByStatusWhenHasNoStatuses()
        {
            // action
            var controller = GetController(GetService(null, null, null, null));
            var result = await controller.GetUnassignedByStatus(string.Empty);

            // assert
            Assert.Equal(400, ((ErrorResult)result).StatusCode);
            Assert.IsType<FieldError>((((ErrorResult)result)).Value);
        }

        [Fact]
        public async void GetUnassignedByTags()
        {
            // arrange
            var fakeRepository = new FakeRepository();
            var filterViews = new[]
            {
                InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                InMemoryFilterView
            };

            filterViews.Select((item, idx) =>
            {
                idx++;
                return new FilterView
                {
                    Id = $"ApplicationId2{idx}",
                    ApplicationId = $"ApplicationNumber00{idx}",
                    Applicant = $"Applicant00{idx}",
                    Ssn = "ABCD1234",
                    Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day),
                    ExpirationDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).AddDays(30),
                    StatusCode = "100.02",
                    Amount = 12000,
                    MaxApproval = 10000,
                    PaymentAmount = 5000,
                    Payout = 5000,
                    SourceId = "SourceId1",
                    SourceType = "SourceType1",
                    Tags = new[] { "tag001", "tag002", "tag003" },
                    Assignees = new[] {
                        new Assignment
                        {
                            Id = "1",
                            AssignedBy = "Eduardo",
                            AssignedOn = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day),
                            Assignee = "Eduardo",
                            EntityId = "EntityId",
                            EntityType = "Application",
                            EscalationId = "EscalationId",
                            Role = "Credit Specialist",
                            TenantId = TenantId
                        }
                    }.ToDictionary(x => x.Role, x => x.Assignee)
                };
            }).ToList().ForEach(item => fakeRepository.Add(item));

            filterViews.Select((item, idx) =>
            {
                idx++;
                return new FilterView
                {
                    Id = $"ApplicationId2{idx}",
                    ApplicationId = $"ApplicationNumber00{idx}",
                    Applicant = $"Applicant00{idx}",
                    Ssn = "ABCD5678",
                    Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day),
                    ExpirationDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).AddDays(30),
                    StatusCode = "100.02",
                    Amount = 12000,
                    MaxApproval = 10000,
                    PaymentAmount = 5000,
                    Payout = 5000,
                    SourceId = "SourceId1",
                    SourceType = "SourceType1",
                    Tags = new[] { "tag1" }
                };
            }).ToList().ForEach(item => fakeRepository.Add(item));

            TokenHandler.Setup(x => x.Parse(It.IsAny<string>()))
                .Returns(InMemoryToken);

            var roles = new[] { "QA", "VP of Sales" };
            IdentityService.Setup(x => x.GetUserRoles(It.IsAny<string>()))
                .ReturnsAsync(roles);

            // action
            var controller = GetController(GetService(fakeRepository, null, null, null, null, null, TokenHandler.Object, IdentityService.Object));
            var result = await controller.GetUnassignedByTags("tag002/tag003");

            // assert
            Assert.Equal(200, ((HttpOkObjectResult)result).StatusCode);
            Assert.NotEmpty(((IEnumerable<IFilterViewUnassigned>)((ObjectResult)result).Value));
            Assert.Equal(filterViews.Count(), ((IEnumerable<IFilterViewUnassigned>)((ObjectResult)result).Value).Count());
            Assert.True(((IEnumerable<IFilterViewUnassigned>)((ObjectResult)result).Value).All(u => u.Last4Ssn == "1234"));
            Assert.True(((IEnumerable<IFilterViewUnassigned>)((ObjectResult)result).Value).All(u => u.ApplicationDate.Date == DateTime.Now.Date));
        }

        [Fact]
        public async void GetUnassignedByStatusWhenHasNoTags()
        {
            // action
            var controller = GetController(GetService(null, null, null, null));
            var result = await controller.GetUnassignedByTags(string.Empty);

            // assert
            Assert.Equal(400, ((ErrorResult)result).StatusCode);
            Assert.IsType<FieldError>((((ErrorResult)result)).Value);
        }

        [Fact]
        public async void GetAllUnassigned()
        {
            // arrange
            var fakeRepository = new FakeRepository();
            var filterViews = new[]
            {
                InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                InMemoryFilterView
            };

            filterViews.Select((item, idx) =>
            {
                idx++;
                return new FilterView
                {
                    Id = $"ApplicationId2{idx}",
                    ApplicationId = $"ApplicationNumber00{idx}",
                    Applicant = $"Applicant00{idx}",
                    Ssn = "ABCD1234",
                    Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day),
                    ExpirationDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).AddDays(30),
                    StatusCode = "100.02",
                    Amount = 12000,
                    MaxApproval = 10000,
                    PaymentAmount = 5000,
                    Payout = 5000,
                    SourceId = "SourceId1",
                    SourceType = "SourceType1",
                    Tags = new[] { "tag001", "tag002", "tag003" },
                    Assignees = new[] {
                        new Assignment
                        {
                            Id = "1",
                            AssignedBy = "Eduardo",
                            AssignedOn = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day),
                            Assignee = "Eduardo",
                            EntityId = "EntityId",
                            EntityType = "Application",
                            EscalationId = "EscalationId",
                            Role = "Credit Specialist",
                            TenantId = TenantId
                        }
                    }.ToDictionary(x => x.Role, x => x.Assignee)
                };
            }).ToList().ForEach(item => fakeRepository.Add(item));

            TokenHandler.Setup(x => x.Parse(It.IsAny<string>()))
                .Returns(InMemoryToken);

            var roles = new[] { "QA" , "VP of Sales" };
            IdentityService.Setup(x => x.GetUserRoles(It.IsAny<string>()))
                .ReturnsAsync(roles);

            // action
            var controller = GetController(GetService(fakeRepository, null, null, null, null, null, TokenHandler.Object, IdentityService.Object));
            var result = await controller.GetAllUnassigned();

            // assert
            Assert.Equal(200, ((HttpOkObjectResult)result).StatusCode);
            Assert.NotEmpty(((IEnumerable<IFilterViewUnassigned>)((ObjectResult)result).Value));
            Assert.Equal(filterViews.Count(), ((IEnumerable<IFilterViewUnassigned>)((ObjectResult)result).Value).Count());
            Assert.True(((IEnumerable<IFilterViewUnassigned>)((ObjectResult)result).Value).All(u => u.Last4Ssn == "1234"));
            Assert.True(((IEnumerable<IFilterViewUnassigned>)((ObjectResult)result).Value).All(u => u.ApplicationDate.Date == DateTime.Now.Date));
        }

        [Fact]
        public async void GetAllAssignedApplicationsWhenDataFound()
        {
            // arrange
            var fakeRepository = new FakeRepository();
            var filterViews = new[] { InMemoryFilterView, InMemoryFilterView, InMemoryFilterView };
            var assigneUser = "meta.slug";
            var assignments = new[] { InMemoryAssignment, InMemoryAssignment, InMemoryAssignment }
            .Select((item, idx) =>
            {
                idx++;
                item.Id = $"Assignment-00{idx}";

                if (idx == 2)
                    item.Role = "QA";

                if (idx == 3)
                {
                    item.Role = "VP of Sales";
                    item.Assignee = assigneUser;
                }

                return item;
            }).ToList();

            filterViews.Select((item, idx) =>
            {
                idx++;
                return new FilterView
                {
                    Id = $"ApplicationId2{idx}",
                    ApplicationId = $"ApplicationNumber00{idx}",
                    Applicant = $"Applicant00{idx}",
                    Ssn = "ABCD1234",
                    Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day),
                    ExpirationDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day).AddDays(30),
                    StatusCode = "100.02",
                    Amount = 12000,
                    MaxApproval = 10000,
                    PaymentAmount = 5000,
                    Payout = 5000,
                    SourceId = "SourceId1",
                    SourceType = "SourceType1",
                    Tags = new[] { "tag001", "tag002", "tag003" },
                    Assignees = assignments
                        .OrderBy(a => a.Assignee)
                        .ToDictionary(x => x.Role, x => x.Assignee, StringComparer.OrdinalIgnoreCase)
            };
            }).ToList().ForEach(item => fakeRepository.Add(item));

            var mockTokenHandler = new Mock<ITokenHandler>();
            mockTokenHandler.Setup(x => x.Parse(It.IsAny<string>())).Returns(new Token
            {
                Subject = assigneUser
            });

            // action
            var controller = GetController(GetService(fakeRepository, null, null, null, null, null, mockTokenHandler.Object));
            var result = await controller.GetAllAssignedApplications();

            // assert
            Assert.Equal(200, ((HttpOkObjectResult)result).StatusCode);
            Assert.NotEmpty(((IEnumerable<IFilterView>)((ObjectResult)result).Value));
            Assert.Equal(filterViews.Count(), ((IEnumerable<IFilterView>)((ObjectResult)result).Value).Count());
        }

      
       

        [Fact]
        public async void GetAllWhenValid()
        {
            // arrange
            var repo = new FakeRepository();
            var sourceType = InMemoryFilterView.SourceType;
            var sourceId = InMemoryFilterView.SourceId;
            var entries = new[]
            {
                InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                InMemoryFilterView
            };
            entries.Select((entry, i) =>
            {
                i++;
                entry.Id = $"ApplicationId{i}";
                entry.ApplicationId = $"ApplicationId{i}";
                entry.Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.AddDays(1).Day);

                return entry;
            }).ToList().ForEach((filterView) =>
            {
                repo.Add(filterView);
            });

            TenantTime.Setup(x => x.TimeZone)
                .Returns(TimeZoneInfo.Utc);

            TenantTime.Setup(x => x.Now)
                .Returns(DateTime.Now);

            // action
            var controller = GetController(GetService(repo, null, null, TenantTime.Object));

            var result = await controller.GetAll ();

            // assert
            Assert.Equal(200, ((HttpOkObjectResult)result).StatusCode);
            Assert.NotNull((IEnumerable<IFilterView>)((HttpOkObjectResult)result).Value);
            Assert.NotEmpty((IEnumerable<IFilterView>)((HttpOkObjectResult)result).Value);
           
        }

        [Fact]
        public async void GetAllByStatusWhenNullStatuses()
        {
            // action
            var controller = GetController(GetService(null, null, null, null));
            var result = await controller.GetAllByStatus(null);

            // assert
            Assert.Equal(400, ((ErrorResult)result).StatusCode);
            Assert.IsType<Error >((((ErrorResult)result)).Value);
        }
        [Fact]
        public async void GetAllByStatusWhenEmptyStatuses()
        {
            // action
            var controller = GetController(GetService(null, null, null, null));
            var result = await controller.GetAllByStatus(string.Empty);

            // assert
            Assert.Equal(400, ((ErrorResult)result).StatusCode);
            Assert.IsType<Error>((((ErrorResult)result)).Value);
        }
        [Fact]
        public async void GetAllByStatusWhen200OK()
        {
            var repo = new FakeRepository();
            var sourceType = InMemoryFilterView.SourceType;
            var sourceId = InMemoryFilterView.SourceId;
            var entries = new[]
            {
                InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                InMemoryFilterView
            };
            entries.Select((entry, i) =>
            {
                i++;
                entry.Id = $"ApplicationId{i}";
                entry.ApplicationId = $"ApplicationId{i}";
                entry.Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.AddDays(1).Day);

                return entry;
            }).ToList().ForEach((filterView) =>
            {
                repo.Add(filterView);
            });

            TenantTime.Setup(x => x.TimeZone)
                .Returns(TimeZoneInfo.Utc);

            TenantTime.Setup(x => x.Now)
                .Returns(DateTime.Now);

            // action
            var controller = GetController(GetService(repo, null, null, TenantTime.Object));

            var result = await controller.GetAllByStatus("A");
            
            // assert
            Assert.Equal(200, ((HttpOkObjectResult)result).StatusCode);
            Assert.NotNull((IEnumerable<IFilterView>)((HttpOkObjectResult)result).Value);
            Assert.NotEmpty((IEnumerable<IFilterView>)((HttpOkObjectResult)result).Value);
        }

        [Fact]
        public async void GetAllByTagsWhenNullTags()
        {
            // action
            var controller = GetController(GetService(null, null, null, null));
            var result = await controller.GetAllByTags(null);

            // assert
            Assert.Equal(400, ((ErrorResult)result).StatusCode);
            Assert.IsType<Error>((((ErrorResult)result)).Value);
        }
        [Fact]
        public async void GetAllByTagsWhenEmptyTags()
        {
            // action
            var controller = GetController(GetService(null, null, null, null));
            var result = await controller.GetAllByTags(string.Empty);

            // assert
            Assert.Equal(400, ((ErrorResult)result).StatusCode);
            Assert.IsType<Error>((((ErrorResult)result)).Value);
        }
        [Fact]
        public async void GetAllByTagsWhen200OK()
        {
            var repo = new FakeRepository();
            var sourceType = InMemoryFilterView.SourceType;
            var sourceId = InMemoryFilterView.SourceId;
            var entries = new[]
            {
                InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                InMemoryFilterView, InMemoryFilterView, InMemoryFilterView,
                InMemoryFilterView
            };
            entries.Select((entry, i) =>
            {
                i++;
                entry.Id = $"ApplicationId{i}";
                entry.ApplicationId = $"ApplicationId{i}";
                entry.Submitted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.AddDays(1).Day);

                return entry;
            }).ToList().ForEach((filterView) =>
            {
                repo.Add(filterView);
            });

            TenantTime.Setup(x => x.TimeZone)
                .Returns(TimeZoneInfo.Utc);

            TenantTime.Setup(x => x.Now)
                .Returns(DateTime.Now);

            // action
            var controller = GetController(GetService(repo, null, null, TenantTime.Object));

            var result = await controller.GetAllByTags("tag1");

            // assert
            Assert.Equal(200, ((HttpOkObjectResult)result).StatusCode);
            Assert.NotNull((IEnumerable<IFilterView>)((HttpOkObjectResult)result).Value);
            Assert.NotEmpty((IEnumerable<IFilterView>)((HttpOkObjectResult)result).Value);
        }

        [Fact]
        public async void GetAssignedByStatusWhenNullUsername()
        {
            // creates a filter view entry in the repository   
            var _sourceType = "merchant";
            var _sourceId = "merchant001";
            var repository = new FakeRepository();

            // filter object 1
            var filter = GetData();
            filter.SourceId = _sourceId;
            filter.SourceType = _sourceType;
            filter.StatusCode = "100.01";
            filter.Assignees = new Dictionary<string, string> { ["kinjal"] = "kinjal" };

            repository.Add(filter);

            // filter object 2
            filter = GetData();
            filter.SourceId = _sourceId;
            filter.SourceType = _sourceType;
            filter.StatusCode = "100.02";
            filter.Assignees = new Dictionary<string, string> { ["kinjal"] = "kinjal" };
            repository.Add(filter);
            // creates a service and gets item to match
            var tokenParser = new Mock<ITokenHandler>();
            tokenParser.Setup(x => x.Parse(It.IsAny<string>()))
                .Returns(new Token { } );

            var service = GetService(repository, null, null, null, null, null, tokenParser.Object);
            // action
            var controller = GetController(service);
            var result = await controller.GetAssignedByStatus(null);

            // assert
            Assert.Equal(400, ((ErrorResult)result).StatusCode);
            Assert.IsType<Error>((((ErrorResult)result)).Value);
        }
        [Fact]
        public async void GetAssignedByStatusWhenNullStatuses()
        {
            // creates a filter view entry in the repository   
            var _sourceType = "merchant";
            var _sourceId = "merchant001";
            var repository = new FakeRepository();

            // filter object 1
            var filter = GetData();
            filter.SourceId = _sourceId;
            filter.SourceType = _sourceType;
            filter.StatusCode = "100.01";
            filter.Assignees = new Dictionary<string, string> { ["kinjal"] = "kinjal" };

            repository.Add(filter);

            // filter object 2
            filter = GetData();
            filter.SourceId = _sourceId;
            filter.SourceType = _sourceType;
            filter.StatusCode = "100.02";
            filter.Assignees = new Dictionary<string, string> { ["kinjal"] = "kinjal" };
            repository.Add(filter);
            // creates a service and gets item to match
            var tokenParser = new Mock<ITokenHandler>();
            tokenParser.Setup(x => x.Parse(It.IsAny<string>()))
                .Returns(new Token { });

            var service = GetService(repository, null, null, null, null, null, tokenParser.Object);
            // action
            var controller = GetController(service);
            var result = await controller.GetAssignedByStatus(null);

            // assert
            Assert.Equal(400, ((ErrorResult)result).StatusCode);
            Assert.IsType<Error>((((ErrorResult)result)).Value);
        }
        [Fact]
        public async void GetAssignedByStatusWhenEmptyStatuses()
        {
            // creates a filter view entry in the repository   
            var _sourceType = "merchant";
            var _sourceId = "merchant001";
            var repository = new FakeRepository();

            // filter object 1
            var filter = GetData();
            filter.SourceId = _sourceId;
            filter.SourceType = _sourceType;
            filter.StatusCode = "100.01";
            filter.Assignees = new Dictionary<string, string> { ["kinjal"] = "kinjal" };

            repository.Add(filter);

            // filter object 2
            filter = GetData();
            filter.SourceId = _sourceId;
            filter.SourceType = _sourceType;
            filter.StatusCode = "100.02";
            filter.Assignees = new Dictionary<string, string> { ["kinjal"] = "kinjal" };
            repository.Add(filter);
            // creates a service and gets item to match
            var tokenParser = new Mock<ITokenHandler>();
            tokenParser.Setup(x => x.Parse(It.IsAny<string>()))
                .Returns(new Token { });

            var service = GetService(repository, null, null, null, null, null, tokenParser.Object);
            // action
            var controller = GetController(service);
            var result = await controller.GetAssignedByStatus (string.Empty);

            // assert
            Assert.Equal(400, ((ErrorResult)result).StatusCode);
            Assert.IsType<Error>((((ErrorResult)result)).Value);
        }
        [Fact]
        public async void GetAssignedByStatusWhen200OK()
        {
            // creates a filter view entry in the repository   
            var _sourceType = "merchant";
            var _sourceId = "merchant001";
            var repository = new FakeRepository();

            // filter object 1
            var filter = GetData();
            filter.SourceId = _sourceId;
            filter.SourceType = _sourceType;
            filter.StatusCode = "100.01";
            filter.Assignees = new Dictionary<string, string> { ["kinjal"] = "kinjal" };

            repository.Add(filter);

            // filter object 2
            filter = GetData();
            filter.SourceId = _sourceId;
            filter.SourceType = _sourceType;
            filter.StatusCode = "100.02";
            filter.Assignees = new Dictionary<string, string> { ["kinjal"] = "kinjal" };
            repository.Add(filter);
            // creates a service and gets item to match
            var tokenParser = new Mock<ITokenHandler>();
            tokenParser.Setup(x => x.Parse(It.IsAny<string>()))
                .Returns(new Token {Subject ="kinjal" });

            var service = GetService(repository, null, null, null, null, null, tokenParser.Object);
            // action
            var controller = GetController(service, Mock.Of<ILogger>());

            var result = await controller.GetAssignedByStatus ("100.01");

            // assert
            Assert.Equal(200, ((HttpOkObjectResult)result).StatusCode);
            Assert.NotNull((IEnumerable<IFilterView>)((HttpOkObjectResult)result).Value);
            Assert.NotEmpty((IEnumerable<IFilterView>)((HttpOkObjectResult)result).Value);
        }



        [Fact]
        public async void GetAssignedByTagsWhenNullUsername()
        {
            // creates a filter view entry in the repository   
            var _sourceType = "merchant";
            var _sourceId = "merchant001";
            var repository = new FakeRepository();

            // filter object 1
            var filter = GetData();
            filter.SourceId = _sourceId;
            filter.SourceType = _sourceType;
            filter.StatusCode = "100.01";
            filter.Assignees = new Dictionary<string, string> { ["kinjal"] = "kinjal" };

            repository.Add(filter);

            // filter object 2
            filter = GetData();
            filter.SourceId = _sourceId;
            filter.SourceType = _sourceType;
            filter.StatusCode = "100.02";
            filter.Assignees = new Dictionary<string, string> { ["kinjal"] = "kinjal" };
            repository.Add(filter);
            // creates a service and gets item to match
            var tokenParser = new Mock<ITokenHandler>();
            tokenParser.Setup(x => x.Parse(It.IsAny<string>()))
                .Returns(new Token { });

            var service = GetService(repository, null, null, null, null, null, tokenParser.Object);
            // action
            var controller = GetController(service, Mock.Of<ILogger>());
            var result = await controller.GetAssignedByTags(null);

            // assert
            Assert.Equal(400, ((ErrorResult)result).StatusCode);
            Assert.IsType<Error>((((ErrorResult)result)).Value);
        }
        [Fact]
        public async void GetAssignedByTagsWhenNullTags()
        {
            // creates a filter view entry in the repository   
            var _sourceType = "merchant";
            var _sourceId = "merchant001";
            var repository = new FakeRepository();

            // filter object 1
            var filter = GetData();
            filter.SourceId = _sourceId;
            filter.SourceType = _sourceType;
            filter.StatusCode = "100.01";
            filter.Assignees = new Dictionary<string, string> { ["kinjal"] = "kinjal" };

            repository.Add(filter);

            // filter object 2
            filter = GetData();
            filter.SourceId = _sourceId;
            filter.SourceType = _sourceType;
            filter.StatusCode = "100.02";
            filter.Assignees = new Dictionary<string, string> { ["kinjal"] = "kinjal" };
            repository.Add(filter);
            // creates a service and gets item to match
            var tokenParser = new Mock<ITokenHandler>();
            tokenParser.Setup(x => x.Parse(It.IsAny<string>()))
                .Returns(new Token { });

            var service = GetService(repository, null, null, null, null, null, tokenParser.Object);
            // action
            var controller = GetController(service, Mock.Of<ILogger>());
            var result = await controller.GetAssignedByTags(null);

            // assert
            Assert.Equal(400, ((ErrorResult)result).StatusCode);
            Assert.IsType<Error>((((ErrorResult)result)).Value);
        }
        [Fact]
        public async void GetAssignedByTagsWhenEmptyTags()
        {
            // creates a filter view entry in the repository   
            var _sourceType = "merchant";
            var _sourceId = "merchant001";
            var repository = new FakeRepository();

            // filter object 1
            var filter = GetData();
            filter.SourceId = _sourceId;
            filter.SourceType = _sourceType;
            filter.StatusCode = "100.01";
            filter.Assignees = new Dictionary<string, string> { ["kinjal"] = "kinjal" };

            repository.Add(filter);

            // filter object 2
            filter = GetData();
            filter.SourceId = _sourceId;
            filter.SourceType = _sourceType;
            filter.StatusCode = "100.02";
            filter.Assignees = new Dictionary<string, string> { ["kinjal"] = "kinjal" };
            repository.Add(filter);
            // creates a service and gets item to match
            var tokenParser = new Mock<ITokenHandler>();
            tokenParser.Setup(x => x.Parse(It.IsAny<string>()))
                .Returns(new Token { });

            var service = GetService(repository, null, null, null, null, null, tokenParser.Object);
            // action
            var controller = GetController(service);
            var result = await controller.GetAssignedByTags(string.Empty);

            // assert
            Assert.Equal(400, ((ErrorResult)result).StatusCode);
            Assert.IsType<Error>((((ErrorResult)result)).Value);
        }
        [Fact]
        public async void GetAssignedByTagsWhen200OK()
        {
            // creates a filter view entry in the repository   
            var _sourceType = "merchant";
            var _sourceId = "merchant001";
            var repository = new FakeRepository();

            // filter object 1
            var filter = GetData();
            filter.SourceId = _sourceId;
            filter.SourceType = _sourceType;
            filter.StatusCode = "100.01";
            filter.Tags = new string[] { "tag1" };
            filter.Assignees = new Dictionary<string, string> { ["kinjal"] = "kinjal" };

            repository.Add(filter);

            // filter object 2
            filter = GetData();
            filter.SourceId = _sourceId;
            filter.SourceType = _sourceType;
            filter.StatusCode = "100.02";
            filter.Tags = new string[] { "tag1" };
            filter.Assignees = new Dictionary<string, string> { ["kinjal"] = "kinjal" };
            repository.Add(filter);
            // creates a service and gets item to match
            var tokenParser = new Mock<ITokenHandler>();
            tokenParser.Setup(x => x.Parse(It.IsAny<string>()))
                .Returns(new Token { Subject = "kinjal" });

            var service = GetService(repository, null, null, null, null, null, tokenParser.Object);
            // action
            var controller = GetController(service);

            var result = await controller.GetAssignedByTags("tag1");

            // assert
            Assert.Equal(200, ((HttpOkObjectResult)result).StatusCode);
            Assert.NotNull((IEnumerable<IFilterView>)((HttpOkObjectResult)result).Value);
            Assert.NotEmpty((IEnumerable<IFilterView>)((HttpOkObjectResult)result).Value);
        }
    }
}