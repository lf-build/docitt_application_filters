﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using System.Collections.Generic;
namespace Docitt.Applications.Filters.Api.Controllers
{
    /// <summary>
    /// Represents api controller class.
    /// </summary>
    [Route("/")]
    public class ApiController : ExtendedController
    {
        /// <summary>
        /// Represents constructor class.
        /// </summary>
        /// <param name="service"></param>
        /// <param name="logger"></param>
        public ApiController(IApplicationFilterService service, ILogger logger) : base(logger)
        {
            Service = service ?? throw new ArgumentException($"{nameof(service)} is madatory");
        }

        private IApplicationFilterService Service { get; }

        /// <summary>
        /// Gets all active loans.
        /// </summary>
        /// <returns></returns>
        [HttpGet("active-loans/all")]
        [ProducesResponseType(typeof(IEnumerable<IFilterView>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetAllActiveLoans()
        {
            return await ExecuteAsync(async () => Ok(await Service.GetAllActiveLoans()));
        }

        /// <summary>
        /// Gets all application loans with REVIEW status(200.10).
        /// </summary>
        /// <returns></returns>
        [HttpGet("application-review-loans/all")]
        [ProducesResponseType(typeof(IEnumerable<IFilterView>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetAllApplicationReviewLoans()
        {
            return await ExecuteAsync(async () => Ok(await Service.GetAllApplicationReviewLoans()));
        }

        /// <summary>
        /// Gets all active loans.
        /// </summary>
        /// <returns></returns>
        [HttpGet("closed-loans/all")]
        [ProducesResponseType(typeof(IEnumerable<IFilterView>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetAllClosedLoans()
        {
            return await ExecuteAsync(async () => Ok(await Service.GetAllClosedLoans()));
        }

        /// <summary>
        /// Gets all by source.
        /// </summary>
        /// <param name="sourceType">Type of the source.</param>
        /// <param name="sourceId">The source identifier.</param>
        /// <returns></returns>
        [HttpGet("{sourceType}/{sourceId}/all")]
        [ProducesResponseType(typeof(IEnumerable<IFilterView>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public IActionResult GetAllBySource(string sourceType, string sourceId)
        {
            return Execute(() => Ok(Service.GetAllBySource(sourceType, sourceId)));
        }

        /// <summary>
        /// Gets the by status.
        /// </summary>
        /// <param name="sourceType">Type of the source.</param>
        /// <param name="sourceId">The source identifier.</param>
        /// <param name="statuses">The statuses.</param>
        /// <returns></returns>
        [HttpGet("{sourceType}/{sourceId}/status/{*statuses}")]
        [ProducesResponseType(typeof(IEnumerable<IFilterView>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public Task<IActionResult> GetByStatus(string sourceType, string sourceId, string statuses)
        {
            return ExecuteAsync(async () =>
            {
                var listStatus = statuses?.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                return Ok(await Service.GetByStatus(sourceType, sourceId, listStatus));
            });
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <returns></returns>
        [HttpGet("all")]
        [ProducesResponseType(typeof(IEnumerable<IFilterView>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetAll()
        {
            return await ExecuteAsync(async () => Ok(await Service.GetAll()));
        }

        /// <summary>
        /// Gets all by status.
        /// </summary>
        /// <param name="statuses">The statuses.</param>
        /// <returns></returns>
        [HttpGet("status/{*statuses}")]
        [ProducesResponseType(typeof(IEnumerable<IFilterView>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetAllByStatus(string statuses)
        {
            return await ExecuteAsync(async () =>
            {
                var listStatus = statuses?.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                return Ok(await Service.GetByStatus(listStatus));
            });
        }

        /// <summary>
        /// Gets the total volume for current month.
        /// </summary>
        /// <param name="sourceType">Type of the source.</param>
        /// <param name="sourceId">The source identifier.</param>
        /// <returns></returns>
        [HttpGet("{sourceType}/{sourceId}/metrics/volume/month")]
        [ProducesResponseType(typeof(double), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetTotalVolumeForCurrentMonth(string sourceType, string sourceId)
        {
            return await ExecuteAsync(async () => Ok(await Service.GetTotalVolumeForCurrentMonthAndStatus(sourceType, sourceId)));
        }

        /// <summary>
        /// Get the average per unit for current month.
        /// </summary>
        /// <param name="sourceType">Type of the source.</param>
        /// <param name="sourceId">The source identifier.</param>
        /// <returns></returns>
        [HttpGet("{sourceType}/{sourceId}/metrics/avg-per-unit/month")]
        [ProducesResponseType(typeof(double), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetAveragePerUnitForCurrentMonth(string sourceType, string sourceId)
        {
            return await ExecuteAsync(async () => Ok(await Service.GetAveragePerUnitForCurrentMonth(sourceType, sourceId)));
        }

        /// <summary>
        /// Get the average per unit for current month.
        /// </summary>
        /// <param name="sourceType">Type of the source.</param>
        /// <param name="sourceId">The source identifier.</param>
        /// <returns></returns>
        [HttpGet("{sourceType}/{sourceId}/metrics/percentage-loan-type/month")]
        [ProducesResponseType(typeof(Dictionary<string,double>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetPercentageLoanTypeForCurrentMonth(string sourceType, string sourceId)
        {
            return await ExecuteAsync(async () => Ok(await Service.GetPercentageLoanTypeForCurrentMonth(sourceType, sourceId)));
        }

        /// <summary>
        /// Gets the by application number.
        /// </summary>
        /// <param name="sourceType">Type of the source.</param>
        /// <param name="sourceId">The source identifier.</param>
        /// <param name="applicationNumber">The application number.</param>
        /// <returns></returns>
        [HttpGet("{sourceType}/{sourceId}/{applicationNumber}")]
        [ProducesResponseType(typeof(IFilterView), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public Task<IActionResult> GetBySourceAndTypeWithApplicationNumber(string sourceType, string sourceId, string applicationNumber)
        {
            return ExecuteAsync(async () => Ok(await Service.GetBySourceAndTypeWithApplicationNumber(sourceType, sourceId, applicationNumber)));
        }

        /// <summary>
        /// Gets the by application number.
        /// </summary>
        /// <param name="sourceType">Type of the source.</param>
       
        /// <param name="applicationNumber">The application number.</param>
        /// <returns></returns>
        [HttpGet("{sourceType}/{applicationNumber}")]
        [ProducesResponseType(typeof(IFilterView), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public Task<IActionResult> GetByApplicationNumber(string sourceType, string applicationNumber)
        {
            return ExecuteAsync(async () => Ok(await Service.GetByApplicationNumber(sourceType,  applicationNumber)));
        }

        /// <summary>
        /// Gets all unassigned.
        /// </summary>
        /// <returns></returns>
        [HttpGet("unassigned/all")]
        [ProducesResponseType(typeof(IEnumerable<IFilterViewUnassigned>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
       
        public Task<IActionResult> GetAllUnassignedApplications()
        {
            return ExecuteAsync(async () => Ok(await Service.GetAllUnassignedApplications()));
        }

        /// <summary>
        /// Gets all assigned applications.
        /// </summary>
        /// <returns></returns>
        [HttpGet("mine")]
        [ProducesResponseType(typeof(IEnumerable<IFilterView>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
      
        public Task<IActionResult> GetAllAssignedApplications()
        {
            return ExecuteAsync(async () => Ok(await Service.GetAllAssignedApplications()));
        }

        /// <summary>
        /// Gets the assigned by status.
        /// </summary>
        /// <param name="statuses">The statuses.</param>
        /// <returns></returns>
        [HttpGet("assigned/status/{*statuses}")]
        [ProducesResponseType(typeof(IEnumerable<IFilterView>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetAssignedByStatus(string statuses)
        {
            return await ExecuteAsync(async () =>
            {
                var listStatus = statuses?.Split(new[] { '/' }, StringSplitOptions.RemoveEmptyEntries);
                return Ok(await Service.GetAssignedByStatus(listStatus));
            });
        }

        /// <summary>
        /// Gets all active loans.
        /// </summary>
        /// <returns></returns>
        [HttpGet("active-loans/mine")]
        [ProducesResponseType(typeof(IEnumerable<IFilterView>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetUserWiseActiveLoans()
        {
            return await ExecuteAsync(async () => Ok(await Service.GetUserWiseActiveLoans()));
        }

        /// <summary>
        /// Gets all application review loans for given user
        /// </summary>
        /// <returns></returns>
        [HttpGet("application-review-loans/mine")]
        [ProducesResponseType(typeof(IEnumerable<IFilterView>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetUserWiseApplicationReviewLoans()
        {
            return await ExecuteAsync(async () => Ok(await Service.GetUserWiseApplicationReviewLoans()));
        }

        /// <summary>
        /// Gets all active loans.
        /// </summary>
        /// <returns></returns>
        [HttpGet("closed-loans/mine")]
        [ProducesResponseType(typeof(IEnumerable<IFilterView>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetUserWiseClosedLoans()
        {
            return await ExecuteAsync(async () => Ok(await Service.GetUserWiseClosedLoans()));
        }

        /// <summary>
        /// Get the TotalCount and TotalAmount by loan type for month.
        /// </summary>       
        /// <returns></returns>
        [HttpGet("metrics/total-count-loan-type/month")]
        [ProducesResponseType(typeof(Dictionary<string,LoanTypeCountAndAmount>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetLoanTypeAmountAndCountForCurrentMonth()
        {
            return await ExecuteAsync(async () => Ok(await Service.GetAmountAndCountByLoanTypeForCurrentMonth()));
        }

        /// <summary>
        /// Gets all suspended loans.
        /// </summary>
        /// <returns></returns>
        [HttpGet("suspended-loans/all")]
        [ProducesResponseType(typeof(IEnumerable<IFilterView>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetAllSuspendedLoans()
        {
            return await ExecuteAsync(async () => Ok(await Service.GetAllSuspendedLoans()));
        }

        /// <summary>
        /// Gets all active and suspended loans.
        /// </summary>
        /// <returns></returns>
        [HttpGet("active-suspended-loans/all")]
        [ProducesResponseType(typeof(IEnumerable<IFilterView>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetAllActiveSuspendedLoans()
        {
            return await ExecuteAsync(async () => Ok(await Service.GetAllActiveSuspendedLoans()));
        }

        /// <summary>
        /// Gets all suspended loans user wise.
        /// </summary>
        /// <returns></returns>
        [HttpGet("suspended-loans/mine")]
        [ProducesResponseType(typeof(IEnumerable<IFilterView>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetUserWiseSuspendedLoans()
        {
            return await ExecuteAsync(async () => Ok(await Service.GetUserWiseSuspendedLoans()));
        }

        /// <summary>
        /// Gets all active and suspended loans user wise.
        /// </summary>
        /// <returns></returns>
        [HttpGet("active-suspended-loans/mine")]
        [ProducesResponseType(typeof(IEnumerable<IFilterView>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetUserWiseActiveSuspendedLoans()
        {
            return await ExecuteAsync(async () => Ok(await Service.GetUserWiseActiveSuspendedLoans()));
        }
        
        
        /// <summary>
        /// Gets all active and closed loans by email.
        /// </summary>
        /// <returns></returns>
        [HttpGet("active-closed-loans/email/{email}")]
        [ProducesResponseType(typeof(IEnumerable<IFilterView>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> GetActiveOrClosedLoansByEmail(string email)
        {
            return await ExecuteAsync(async () => Ok(await Service.GetActiveOrClosedLoansByEmail(email)));
        }
    }
}