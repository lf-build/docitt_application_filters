﻿using Docitt.ReminderService.Client;
using Docitt.ReminderService.Services;
using Docitt.Application;
using Docitt.Application.Client;
using LendFoundry.Configuration;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;
using LendFoundry.StatusManagement;
using LendFoundry.StatusManagement.Client;
using LendFoundry.Tenant.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using Docitt.AssignmentEngine.Client;
using Docitt.AssignmentEngine;
using LendFoundry.Foundation.Listener;

namespace Docitt.Applications.Filters
{
    public class ApplicationFilterListener : ListenerBase, IApplicationFilterListener
    {
        public ApplicationFilterListener
        (
            IConfigurationServiceFactory configurationFactory,
            ITokenHandler tokenHandler,
            IEventHubClientFactory eventHubFactory,
            IFilterViewRepositoryFactory repositoryFactory,
            ILoggerFactory loggerFactory,
            ITenantServiceFactory tenantServiceFactory,
            IApplicationServiceClientFactory applicationServiceFactory,
            IStatusManagementServiceFactory statusManagementFactory,
                IReminderServiceClientFactory reminderServiceFactory,
                IAssignmentServiceClientFactory assignmentFactory
        ) : base( tokenHandler, eventHubFactory, loggerFactory, tenantServiceFactory, Settings.ServiceName)

        {
            EventHubFactory = eventHubFactory ?? throw new ArgumentException($"{nameof(eventHubFactory)} is mandatory");
            ConfigurationFactory = configurationFactory ?? throw new ArgumentException($"{nameof(configurationFactory)} is mandatory");
            TokenHandler = tokenHandler ?? throw new ArgumentException($"{nameof(tokenHandler)} is mandatory");
            RepositoryFactory = repositoryFactory ?? throw new ArgumentException($"{nameof(repositoryFactory)} is mandatory");
            ApplicationServiceFactory = applicationServiceFactory ?? throw new ArgumentException($"{nameof(applicationServiceFactory)} is mandatory");

            StatusManagementFactory = statusManagementFactory ?? throw new ArgumentException($"{nameof(statusManagementFactory)} is mandatory");
            ReminderServiceFactory = reminderServiceFactory ?? throw new ArgumentException($"{nameof(reminderServiceFactory)} is mandatory");
            AssignmentFactory = assignmentFactory ?? throw new ArgumentException($"{nameof(assignmentFactory)} is mandatory");
        }

        private IAssignmentServiceClientFactory AssignmentFactory { get; }
        private IStatusManagementServiceFactory StatusManagementFactory { get; }

        private IApplicationServiceClientFactory ApplicationServiceFactory { get; }

        private IConfigurationServiceFactory ConfigurationFactory { get; }

        private IEventHubClientFactory EventHubFactory { get; }

        private ITokenHandler TokenHandler { get; }

        private IFilterViewRepositoryFactory RepositoryFactory { get; }

        private IReminderServiceClientFactory ReminderServiceFactory { get; }

        public override List<string> GetEventNames(string tenant)
        {
            var token = TokenHandler.Issue(tenant, Settings.ServiceName);
            var reader = new StaticTokenReader(token.Value);
            var configurationService = ConfigurationFactory.Create<Configuration>(Settings.ServiceName, reader);
            configurationService.ClearCache(Settings.ServiceName);
            var configuration = configurationService.Get();
            return configuration?.Events.Select(evnt => evnt.Name).Distinct().ToList();
        }

        public override List<string> SubscribeEvents(string tenant, ILogger logger)
        {
            var token = TokenHandler.Issue(tenant, Settings.ServiceName);
            var reader = new StaticTokenReader(token.Value);
            
            var eventhub = EventHubFactory.Create(reader);
            var reminderService = ReminderServiceFactory.Create(reader);
            var assignmentService = AssignmentFactory.Create(reader);
            var statusManagementService = StatusManagementFactory.Create(reader);

            var repository = RepositoryFactory.Create(reader);
            var applicationService = ApplicationServiceFactory.Create(reader);


            var configurationService = ConfigurationFactory.Create<Configuration>(Settings.ServiceName, reader);
            configurationService.ClearCache(Settings.ServiceName);
            var configuration = configurationService.Get();
            if (configuration == null)
            {
                logger.Error($"The configuration for service #{Settings.ServiceName} could not be found for {tenant} , please verify");
                return null;
            }
            else
            {
                logger.Info($"#{configuration.Events.Length} evvents found from configuration: {Settings.ServiceName}");
                var uniqueEvents = configuration
                        .Events
                         .Distinct()
                        .ToList();

                uniqueEvents
                      .ForEach(eventConfig =>
                      {
                          eventhub.On(eventConfig.Name, AddView(eventConfig, repository, logger, applicationService, statusManagementService, reminderService, assignmentService));

                          logger.Info($"It was made subscription to EventHub with the Event: #{eventConfig.Name} for tenant {tenant}");

                      });


                return uniqueEvents.Select(p => p.Name).Distinct().ToList();
                
            }
        }


      

        private static Action<EventInfo> AddView
        (
            EventMapping eventConfiguration,
            IFilterViewRepository repository,
            ILogger logger,
            IApplicationService applicationService,
            IEntityStatusService statusManagementService,
            IReminderService reminderService,
           IAssignmentService assignmentService
        )
        {
            return @event =>
            {
                try
                {
                    var applicationNumber = eventConfiguration.ApplicationNumber.FormatWith(@event);

                    var application = applicationService.GetByApplicationNumber(applicationNumber);
                    if (application != null)
                    {
                        if (application.Applicants == null || !application.Applicants.Any())
                        {
                            logger.Warn($"No applicants found to application #{applicationNumber}");
                            return;
                        }

                        // snapshot creation
                        // var filterView = new FilterView();

                        // application source values
                        //var primaryApplicant = application.Applicants.FirstOrDefault(a => a.IsPrimary);
                        //var coBorrower = application.Applicants.FirstOrDefault(a => !a.IsPrimary);

                        var filterView = repository.GetByApplicationNumber(applicationNumber).Result ?? new FilterView {ApplicationId = application.ApplicationNumber};

                        var applicants = new List<IApplicantFilterView>();

                        //if (primaryApplicant != null)
                        //    applicants.Add(SetAppicantFilterView(primaryApplicant));
                        //if (coBorrower != null)
                        //    applicants.Add(SetAppicantFilterView(coBorrower));

                        foreach (var applicant in application.Applicants)
                        {
                            applicants.Add(SetAppicantFilterView(applicant));
                        }

                        filterView.Applicants = applicants;
                        filterView.Submitted = application.SubmittedDate.Time;
                        filterView.SourceId = application.Source.Id;
                        filterView.SourceType = application.Source.Type.ToString().ToLower();
                        filterView.Amount = application.Amount;
                        filterView.PropertyValue = application.PropertyValue;
                        filterView.PropertyAddress = application.PropertyAddress;

                        if (application.ExpirationDate != null)
                            filterView.ExpirationDate = application.ExpirationDate.Time;

                        filterView.DaysToClose = (filterView.ExpirationDate - filterView.Submitted).TotalDays;

                        filterView.LoanType = application.Purpose;

                        filterView.TotalUnReadReminders = reminderService
                            .GetUnReadReminderCount(Constants.Application, applicationNumber, string.Empty).Result;

                        // gets all application assignees
                        var assignees = new Dictionary<string, string>();
                        var assignments = assignmentService.Get("Application", application.ApplicationNumber).Result
                            ?.ToList();

                        if (assignments != null && assignments.Any())
                        {
                            foreach (var asgn in assignments)
                            {
                                if (!assignees.ContainsKey(asgn.Assignee))
                                    assignees.Add(asgn.Assignee, asgn.Role);
                                else
                                {
                                    logger.Warn(
                                        $"Duplicate assignee {asgn.Assignee} in {application.ApplicationNumber}");
                                }
                            }

                            //assignees = assignments.ToDictionary(x => x.Assignee, x => x.Role);
                        }
                        else
                        {
                            logger.Debug(
                                $"No assignments found for this snapshot using EntityId#{application.ApplicationNumber}");
                        }

                        filterView.Assignees = assignees;

                        var applicationStatus = statusManagementService
                            .GetStatusByEntity(Constants.Application, applicationNumber).Result;

                        // status management source values
                        if (applicationStatus != null)
                        {
                            if (filterView.Status != null && applicationStatus.Code != filterView.Status.Code)
                            {
                                if (filterView.CompletedStatuses == null)
                                    filterView.CompletedStatuses = new List<Status>();


                                if (filterView.CompletedStatuses.All(p => p.Code != filterView.Status.Code))
                                {
                                    filterView.CompletedStatuses.Add(new Status
                                    {
                                        Code = filterView.Status.Code,
                                        Name = filterView.Status.Name,
                                        Label = filterView.Status.Label
                                    });
                                }
                            }


                            filterView.Status = new Status
                            {
                                Code = applicationStatus.Code,
                                Name = applicationStatus.Name,
                                Label = applicationStatus.Label
                            };


                            filterView.ActiveOn = applicationStatus.ActiveOn;

                            logger.Info(
                                $"Application status found for {applicationNumber} snapshot #{filterView.Status.Code} #{filterView.Status.Name}");
                        }
                        else
                            logger.Info($"No application status found for {applicationNumber} snapshot");

                        repository.AddOrUpdate(filterView);
                        
                        logger.Info($"New snapshot added to application {applicationNumber} for event {@event.Name}");
                    }
                    else
                        logger.Warn(
                            $"Application #{applicationNumber} could not be found while processing subscription #{@event.Name}");
                }
                catch (Exception ex)
                {
                    logger.Error($"Unhandled exception while listening event {@event.Name} for {@event?.TenantId}", ex, @event);
                }
            };
        }

        private static IApplicantFilterView SetAppicantFilterView(IApplicantRequest applicant)
        {
            var applicantFilterView = new ApplicantFilterView
            {
                ApplicantId = applicant.Id,
                FirstName = applicant.FirstName,
                LastName = applicant.LastName,
                Email = applicant.Email,
                Name = $"{applicant.FirstName} {applicant.LastName}",
                Ssn = applicant.Ssn,
                IsPrimary = applicant.IsPrimary,
                UserName =  (applicant.UserIdentity != null)? applicant.UserIdentity.UserId : string.Empty ,
                ApplicantType = applicant.ApplicantType
            };
            if (applicant.PhoneNumbers != null && applicant.PhoneNumbers.Any())
            {
                var primaryPhoneNumber = applicant.PhoneNumbers.FirstOrDefault(p => p.IsPrimary);
                applicantFilterView.PhoneNumber = primaryPhoneNumber?.Number;
                applicantFilterView.Extension = primaryPhoneNumber?.Extension;
            }
            var address = string.Empty;
            var city = string.Empty;
            var state = string.Empty;
            var zipCode = string.Empty;
            var country = string.Empty;
            if (applicant.Addresses != null && applicant.Addresses.Any())
            {
                var primaryAddress = applicant.Addresses.FirstOrDefault(p => p.IsPrimary);

                if (!string.IsNullOrEmpty(primaryAddress?.Line1))
                    address = $"{address}{primaryAddress.Line1}";
                if (!string.IsNullOrEmpty(primaryAddress?.Line2))
                    address = $"{address} {primaryAddress.Line2}";
                if (!string.IsNullOrEmpty(primaryAddress?.Line3))
                    address = $"{address} {primaryAddress.Line3}";

                if (!string.IsNullOrEmpty(primaryAddress?.City))
                    city = primaryAddress.City;
                if (!string.IsNullOrEmpty(primaryAddress?.State))
                    state = primaryAddress.State;

                if (!string.IsNullOrEmpty(primaryAddress?.Country))
                    country = primaryAddress.Country;
                if (!string.IsNullOrEmpty(primaryAddress?.ZipCode))
                    zipCode = primaryAddress.ZipCode;
            }
            applicantFilterView.Address = address;
            applicantFilterView.City = city;
            applicantFilterView.State = state;

            applicantFilterView.Country = country;
            applicantFilterView.ZipCode = zipCode;
            return applicantFilterView;
        }
    }
}