﻿using Docitt.Application;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;

using LendFoundry.Security.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LendFoundry.Security.Identity.Client;
namespace Docitt.Applications.Filters
{
    public class ApplicationFilterService : IApplicationFilterService
    {
        public ApplicationFilterService
        (
            IFilterViewRepository repository,
            ILogger logger,
            ITenantTime tenantTime,
            Configuration configuration,
            ITokenReader tokenReader,
            ITokenHandler tokenParser,
            IIdentityService identityService
        )
        {
            Repository = repository ?? throw new ArgumentException($"{nameof(repository)} is mandatory");
            Logger = logger ?? throw new ArgumentException($"{nameof(logger)} is mandatory");
            TenantTime = tenantTime ?? throw new ArgumentException($"{nameof(tenantTime)} is mandatory");
            Configuration = configuration ?? throw new ArgumentException($"{nameof(configuration)} is mandatory");
            TokenReader = tokenReader ?? throw new ArgumentException($"{nameof(tokenReader)} is mandatory");
            TokenParser = tokenParser ?? throw new ArgumentException($"{nameof(tokenParser)} is mandatory");
            IdentityService = identityService ?? throw new ArgumentException($"{nameof(identityService)} is mandatory");
        }

        private ITokenReader TokenReader { get; }

        private ITokenHandler TokenParser { get; }

        private Configuration Configuration { get; }

        private IFilterViewRepository Repository { get; }

        private ILogger Logger { get; }

        private ITenantTime TenantTime { get; }

        private IIdentityService IdentityService { get; }

        public IEnumerable<IFilterView> GetAllBySource(string sourceType, string sourceId)
        {
            Validate(sourceType, sourceId);

            return Repository.GetAllBySource(sourceType, sourceId);
        }

        public async Task<IEnumerable<IFilterView>> GetAllActiveLoans()
        {
            var todayDate = TenantTime.Today;
            if (Configuration?.StatusesActiveLoans == null || !Configuration.StatusesActiveLoans.Any())
                throw new ArgumentException($"{nameof(Configuration.StatusesActiveLoans)} is mandatory in configuration");
            return await Repository.GetAllActiveLoans(todayDate, Configuration.StatusesActiveLoans);
        }

        public async Task<IEnumerable<IFilterView>> GetAllApplicationReviewLoans()
        {
            var todayDate = TenantTime.Today;
            if (Configuration?.StatusesApplicationReviewLoans == null || !Configuration.StatusesApplicationReviewLoans.Any())
                throw new ArgumentException($"{nameof(Configuration.StatusesApplicationReviewLoans)} is mandatory in configuration");
            return await Repository.GetAllApplicationReviewLoans(todayDate, Configuration.StatusesApplicationReviewLoans);
        }

        public async Task<IEnumerable<IFilterView>> GetAllClosedLoans()
        {
            var todayDate = TenantTime.Today;
            if (Configuration?.StatusesClosedLoans == null || !Configuration.StatusesClosedLoans.Any())
                throw new ArgumentException($"{nameof(Configuration.StatusesClosedLoans)} is mandatory in configuration");
            return await Repository.GetAllClosedLoans(todayDate, Configuration.StatusesClosedLoans);
        }

        public Task<IEnumerable<IFilterView>> GetByStatus(string sourceType, string sourceId, IEnumerable<string> statuses)
        {
            try
            {
                Validate(sourceType, sourceId);

                if (statuses == null || !statuses.Any())
                    throw new ArgumentException($"{nameof(statuses)} is mandatory");

                return Repository.GetByStatus(sourceType, sourceId, statuses);
            }
            catch (Exception ex)
            {
                Logger.Error($"The method GetByStatus({sourceType}, {sourceId}, {statuses}) raised an error : {ex.Message}\n");
                throw;
            }
        }

        private void ValidateEntityType(string sourceType)
        {
            if (sourceType.Equals(SourceType.Undefined.ToString(), StringComparison.InvariantCultureIgnoreCase))
                throw new InvalidArgumentException($"Source {nameof(sourceType)} is not valid", EnumExtensions.GetEnumNames<SourceType>());
        }

        public Task<IEnumerable<IFilterView>> GetAll()
        {
            return Repository.GetAll();
        }

        public Task<IEnumerable<IFilterView>> GetByStatus(IEnumerable<string> statuses)
        {
            if (statuses == null || !statuses.Any())
                throw new ArgumentException($"{nameof(statuses)} is mandatory");
            var statusList = statuses as IList<string> ?? statuses.ToList();
            return Repository.GetByStatus(statusList);
        }

        public async Task<double> GetTotalVolumeForCurrentMonthAndStatus(string sourceType, string sourceId)
        {
            try
            {
                Validate(sourceType, sourceId);

                var firstDayOfMonth = new DateTimeOffset(new DateTime(TenantTime.Now.Year, TenantTime.Now.Month, 1, 0, 0, 0));
                return await Repository.GetTotalVolumeForCurrentMonthAndStatus(sourceType, sourceId, firstDayOfMonth, Configuration.StatusesClosedLoans);
            }
            catch (Exception ex)
            {
                Logger.Error($"The method GetTotalVolumeForCurrentMonthAndStatus({sourceType}, {sourceId}) raised an error: {ex.Message}\n");
                throw;
            }
        }

        public async Task<double> GetAveragePerUnitForCurrentMonth(string sourceType, string sourceId)
        {
            try
            {
                Validate(sourceType, sourceId);

                var firstDayOfMonth = new DateTimeOffset(new DateTime(TenantTime.Now.Year, TenantTime.Now.Month, 1, 0, 0, 0));
                return await Repository.GetAveragePerUnitForCurrentMonth(sourceType, sourceId, firstDayOfMonth, Configuration.StatusesClosedLoans);
            }
            catch (Exception ex)
            {
                Logger.Error($"The method GetAveragePerUnitForCurrentMonth({sourceType}, {sourceId}) raised an error: {ex.Message}\n");
                throw;
            }
        }

        public async Task<Dictionary<string, double>> GetPercentageLoanTypeForCurrentMonth(string sourceType, string sourceId)
        {
            try
            {
                Validate(sourceType, sourceId);

                var firstDayOfMonth = new DateTimeOffset(new DateTime(TenantTime.Now.Year, TenantTime.Now.Month, 1, 0, 0, 0));
                return await Repository.GetPercentageLoanTypeForCurrentMonth(sourceType, sourceId, firstDayOfMonth, Configuration.StatusesClosedLoans);
            }
            catch (Exception ex)
            {
                Logger.Error($"The method GetPercentageLoanTypeForCurrentMonth({sourceType}, {sourceId}) raised an error: {ex.Message}\n");
                throw;
            }
        }
        public Task<IFilterView> GetBySourceAndTypeWithApplicationNumber(string sourceType, string sourceId, string applicationNumber)
        {
            try
            {
                Validate(sourceType, sourceId);

                if (string.IsNullOrWhiteSpace(applicationNumber))
                    throw new InvalidArgumentException($"{nameof(applicationNumber)} is mandatory", nameof(applicationNumber));

                var result = Repository.GetBySourceAndTypeWithApplicationNumber(sourceType, sourceId, applicationNumber);
                if (result.Result == null)
                    throw new NotFoundException($"The snapshot could not be found with SourceType:{sourceType}, SourceId:{sourceId}, ApplicationNumber:{applicationNumber}");

                return result;
            }
            catch (Exception ex)
            {
                Logger.Error($"The method GetByApplicationNumber({sourceType}, {sourceId}, {applicationNumber}) raised an error : {ex.Message}\n");
                throw;
            }
        }
        public Task<IFilterView> GetByApplicationNumber(string sourceType,  string applicationNumber)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(sourceType))
                    throw new InvalidArgumentException($"{nameof(sourceType)} is mandatory", nameof(sourceType));

                ValidateEntityType(sourceType);

                if (string.IsNullOrWhiteSpace(applicationNumber))
                    throw new InvalidArgumentException($"{nameof(applicationNumber)} is mandatory", nameof(applicationNumber));

                var result = Repository.GetByApplicationNumber(sourceType,  applicationNumber);
                if (result.Result == null)
                    throw new NotFoundException($"The snapshot could not be found with SourceType:{sourceType},  ApplicationNumber:{applicationNumber}");

                return result;
            }
            catch (Exception ex)
            {
                Logger.Error($"The method GetByApplicationNumber({sourceType}, {applicationNumber}) raised an error : {ex.Message}\n");
                throw;
            }
        }

        private void Validate(string sourceType, string sourceId)
        {
            if (string.IsNullOrWhiteSpace(sourceType))
                throw new InvalidArgumentException($"{nameof(sourceType)} is mandatory", nameof(sourceType));

            if (string.IsNullOrWhiteSpace(sourceId))
                throw new InvalidArgumentException($"{nameof(sourceId)} is mandatory", nameof(sourceId));

            ValidateEntityType(sourceType);
        }


        public async Task<IEnumerable<IFilterViewUnassigned>> GetAllUnassignedApplications()
        {
            var roles = await GetUserRoles();

            return await Repository.GetAllUnassigned(roles);
        }

        private async Task<IEnumerable<string>> GetUserRoles()
        {
            var userName = GetTokenUserName();

            if (string.IsNullOrWhiteSpace(userName))
                throw new ArgumentException("Token user cannot be found in the request, please verify");

            var roles = await IdentityService.GetUserRoles(userName);
            if (roles == null || roles.Any() == false)
                throw new NotFoundException("Could not be found the roles from current user");

            return roles;
        }

        public Task<IEnumerable<IFilterView>> GetAllAssignedApplications()
        {
            var userName = GetTokenUserName();

            if (string.IsNullOrWhiteSpace(userName))
                throw new ArgumentException("Token user cannot be found in the request, please verify");

            return Repository.GetAllAssignedApplications(userName);
        }

        public Task<IEnumerable<IFilterView>> GetAssignedByStatus(IEnumerable<string> statuses)
        {
            var userName = GetTokenUserName();

            if (string.IsNullOrWhiteSpace(userName))
                throw new ArgumentException("Token user cannot be found in the request, please verify");

            if (statuses == null || !statuses.Any())
                throw new InvalidArgumentException($"{nameof(statuses)} is mandatory", nameof(statuses));
            var statusesList = statuses as IList<string> ?? statuses.ToList();
            return Repository.GetAssignedByStatus(userName, statusesList);
        }

        public async Task<IEnumerable<IFilterView>> GetUserWiseActiveLoans()
        {   
            var userName = GetTokenUserName();

            if (string.IsNullOrWhiteSpace(userName))
                throw new ArgumentException("Token user cannot be found in the request, please verify");

            if (Configuration?.StatusesActiveLoans == null || !Configuration.StatusesActiveLoans.Any())
                throw new ArgumentException($"{nameof(Configuration.StatusesActiveLoans)} is mandatory in configuration");

            return await Repository.GetUserWiseActiveLoans(userName, Configuration.StatusesActiveLoans);
        }

        public async Task<IEnumerable<IFilterView>> GetUserWiseApplicationReviewLoans()
        {   
            var userName = GetTokenUserName();

            if (string.IsNullOrWhiteSpace(userName))
                throw new ArgumentException("Token user cannot be found in the request, please verify");

            if (Configuration?.StatusesApplicationReviewLoans == null || !Configuration.StatusesApplicationReviewLoans.Any())
                throw new ArgumentException($"{nameof(Configuration.StatusesApplicationReviewLoans)} is mandatory in configuration");

            return await Repository.GetUserWiseApplicationReviewLoans(userName, Configuration.StatusesApplicationReviewLoans);
        }

        public async Task<IEnumerable<IFilterView>> GetUserWiseClosedLoans()
        {   
            var userName = GetTokenUserName();

            if (string.IsNullOrWhiteSpace(userName))
                throw new ArgumentException("Token user cannot be found in the request, please verify");


            if (Configuration?.StatusesClosedLoans == null || !Configuration.StatusesClosedLoans.Any())
                throw new ArgumentException($"{nameof(Configuration.StatusesClosedLoans)} is mandatory in configuration");
            return await Repository.GetUserWiseClosedLoans(userName, Configuration.StatusesClosedLoans);
        }

        public async Task<Dictionary<string, LoanTypeCountAndAmount>> GetAmountAndCountByLoanTypeForCurrentMonth()
        {
            try
            {
                var userName = GetTokenUserName();

                if (string.IsNullOrWhiteSpace(userName))
                    throw new ArgumentException("Token user cannot be found in the request, please verify");


                var startDate = new DateTimeOffset(new DateTime(TenantTime.Now.Year, TenantTime.Now.Month, 1, 0, 0, 0));
                var endDate = new DateTimeOffset(new DateTime(TenantTime.Now.Year, TenantTime.Now.Month, DateTime.DaysInMonth(TenantTime.Now.Year, TenantTime.Now.Month), 0, 0, 0));


                return await Repository.GetTotalVolumeAndLoanCountByLoanTypeAndStatus(userName, startDate, endDate, Configuration.StatusesClosedLoans);
            }
            catch (Exception ex)
            {
                Logger.Error($"The method GetTotalVolumeForCurrentMonthAndStatus raised an error: {ex.Message}\n");
                throw;
            }
        }
        private string GetTokenUserName()
        {
            var token = TokenParser.Parse(TokenReader.Read());
            return token.Subject;
        }        
        public async Task<IEnumerable<IFilterView>> GetAllSuspendedLoans()
        {
            if (Configuration?.StatusesSuspendedLoans == null || !Configuration.StatusesSuspendedLoans.Any())
                throw new ArgumentException($"{nameof(Configuration.StatusesSuspendedLoans)} is mandatory in configuration");
            return await Repository.GetAllSuspendedLoans(Configuration.StatusesSuspendedLoans);
        }

        public async Task<IEnumerable<IFilterView>> GetAllActiveSuspendedLoans()
        {
            var todayDate = TenantTime.Today;            
            if (Configuration?.StatusesActiveLoans == null || !Configuration.StatusesActiveLoans.Any())
                throw new ArgumentException($"{nameof(Configuration.StatusesActiveLoans)} is mandatory in configuration");
            if (Configuration?.StatusesSuspendedLoans == null || !Configuration.StatusesSuspendedLoans.Any())
                throw new ArgumentException($"{nameof(Configuration.StatusesSuspendedLoans)} is mandatory in configuration");

            return await Repository.GetAllActiveSuspendedLoans(todayDate, Configuration.StatusesActiveLoans, Configuration.StatusesSuspendedLoans);            
        }

        public async Task<IEnumerable<IFilterView>> GetUserWiseSuspendedLoans()
        {   
            var userName = GetTokenUserName();

            if (string.IsNullOrWhiteSpace(userName))
                throw new ArgumentException("Token user cannot be found in the request, please verify");

            if (Configuration?.StatusesSuspendedLoans == null || !Configuration.StatusesSuspendedLoans.Any())
                throw new ArgumentException($"{nameof(Configuration.StatusesSuspendedLoans)} is mandatory in configuration");

            return await Repository.GetUserWiseSuspendedLoans(userName, Configuration.StatusesSuspendedLoans);
        }

        public async Task<IEnumerable<IFilterView>> GetUserWiseActiveSuspendedLoans()
        {   
            var userName = GetTokenUserName();

            if (string.IsNullOrWhiteSpace(userName))
                throw new ArgumentException("Token user cannot be found in the request, please verify");

            if (Configuration?.StatusesActiveLoans == null || !Configuration.StatusesActiveLoans.Any())
                throw new ArgumentException($"{nameof(Configuration.StatusesActiveLoans)} is mandatory in configuration");

            if (Configuration?.StatusesSuspendedLoans == null || !Configuration.StatusesSuspendedLoans.Any())
                throw new ArgumentException($"{nameof(Configuration.StatusesSuspendedLoans)} is mandatory in configuration");

            return await Repository.GetUserWiseActiveSuspendedLoans(userName, Configuration.StatusesActiveLoans, Configuration.StatusesSuspendedLoans);
        }

        public async Task<IEnumerable<IFilterView>> GetActiveOrClosedLoansByEmail(string email)
        {            
            if (string.IsNullOrWhiteSpace(email))
                throw new ArgumentException("Token user cannot be found in the request, please verify");

            if (Configuration?.StatusesActiveLoans == null || !Configuration.StatusesActiveLoans.Any())
                throw new ArgumentException(
                    $"{nameof(Configuration.StatusesActiveLoans)} is mandatory in configuration");

            if (Configuration?.StatusesClosedLoans == null || !Configuration.StatusesClosedLoans.Any())
                throw new ArgumentException(
                    $"{nameof(Configuration.StatusesClosedLoans)} is mandatory in configuration");

            return await Repository.GetActiveOrClosedLoansByEmail(email, Configuration.StatusesActiveLoans,
                Configuration.StatusesClosedLoans);
        }
    }
}