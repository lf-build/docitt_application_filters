﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
namespace Docitt.Applications.Filters
{
    public static class ApplicationFilterListenerExtensions
    {
        public static void UseApplicationFilterListener(this IApplicationBuilder app)
        {
            app.ApplicationServices.GetRequiredService<IApplicationFilterListener>().Start();
        }
    }
}