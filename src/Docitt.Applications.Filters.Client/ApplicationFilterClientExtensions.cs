﻿using LendFoundry.Security.Tokens;
using Microsoft.Extensions.DependencyInjection;
using System;
namespace Docitt.Applications.Filters.Client
{
    public static class ApplicationFilterClientExtensions
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddApplicationFilters(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IApplicationFilterClientFactory>(p => new ApplicationFilterClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IApplicationFilterClientFactory>().Create(p.GetService<ITokenReader>()));

            return services;
        }

        public static IServiceCollection AddApplicationFilters(this IServiceCollection services, Uri uri)
        {
            services.AddTransient<IApplicationFilterClientFactory>(p => new ApplicationFilterClientFactory(p, uri));
            services.AddTransient(p => p.GetService<IApplicationFilterClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddApplicationFilters(this IServiceCollection services)
        {
            services.AddTransient<IApplicationFilterClientFactory>(p => new ApplicationFilterClientFactory(p));
            services.AddTransient(p => p.GetService<IApplicationFilterClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        
    }
}