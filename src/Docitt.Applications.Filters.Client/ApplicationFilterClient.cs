﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using LendFoundry.Foundation.Services;

namespace Docitt.Applications.Filters.Client
{
    public class ApplicationFilterClient : IApplicationFilterService
    {
        public ApplicationFilterClient(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }

        public IEnumerable<IFilterView> GetAllBySource(string sourceType, string sourceId)
        {
            Validate(sourceType, sourceId);
            return Client.GetAsync<List<FilterView>>($"/{sourceType}/{sourceId}/all").Result;
        }

        public async Task<IEnumerable<IFilterView>> GetByStatus(string sourceType, string sourceId, IEnumerable<string> statuses)
        {
            Validate(sourceType, sourceId);
            var uri = $"/{sourceType}/{sourceId}/status/";
            uri = uri + string.Join("/", statuses);
            return await Client.GetAsync<List<FilterView>>(uri);
        }

        private void Validate(string sourceType, string sourceId)
        {
            if (string.IsNullOrWhiteSpace(sourceType))
                throw new InvalidArgumentException($"{nameof(sourceType)} is mandatory", nameof(sourceType));

            if (string.IsNullOrWhiteSpace(sourceId))
                throw new InvalidArgumentException($"{nameof(sourceId)} is mandatory", nameof(sourceId));
        }

        public async Task<IEnumerable<IFilterView>> GetAll()
        {
            return await Client.GetAsync<List<FilterView>>("/all");
        }

        public async Task<IEnumerable<IFilterView>> GetByStatus(IEnumerable<string> statuses)
        {
            var uri = "/status/";
            uri = uri + string.Join("/", statuses);
            return await Client.GetAsync<List<FilterView>>(uri);
        }

        public async Task<IEnumerable<IFilterView>> GetAllActiveLoans()
        {
           return await Client.GetAsync<List<FilterView>>("active-loans/all");
        }

        public async Task<IEnumerable<IFilterView>> GetAllApplicationReviewLoans()
        {
           return await Client.GetAsync<List<FilterView>>("application-review-loans/all");
        }

        public async Task<IEnumerable<IFilterView>> GetAllClosedLoans()
        {
            return await Client.GetAsync<List<FilterView>>("closed-loans/all");
        }

        public async Task<double> GetTotalVolumeForCurrentMonthAndStatus(string sourceType, string sourceId)
        {
            var response =   await Client.GetStringAsync($"{sourceType}/{sourceId}/metrics/volume/month");
            return Convert.ToDouble(response);
        }

        public async Task<double> GetAveragePerUnitForCurrentMonth(string sourceType, string sourceId)
        {
            var response =   await Client.GetStringAsync($"{sourceType}/{sourceId}/metrics/avg-per-unit/month");
            return Convert.ToDouble(response);
        }

        public async Task<Dictionary<string, double>> GetPercentageLoanTypeForCurrentMonth(string sourceType, string sourceId)
        {
            return await Client.GetAsync<Dictionary<string, double>>($"{sourceType}/{sourceId}/metrics/percentage-loan-type/month");
        }

        public async Task<IFilterView> GetBySourceAndTypeWithApplicationNumber(string sourceType, string sourceId, string applicationNumber)
        {
            Validate(sourceType, sourceId);

            if (string.IsNullOrWhiteSpace(applicationNumber))
                throw new InvalidArgumentException($"{nameof(applicationNumber)} is mandatory", nameof(applicationNumber));

            return await Client.GetAsync<FilterView>($"/{sourceType}/{sourceId}/{applicationNumber}");
        }

        public async Task<IFilterView> GetByApplicationNumber(string sourceType,  string applicationNumber)
        {
            return await Client.GetAsync<FilterView>($"/{sourceType}/{applicationNumber}");
        }

        public async Task<IEnumerable<IFilterViewUnassigned>> GetAllUnassignedApplications()
        {
            return await Client.GetAsync<List<FilterViewUnassigned>>("/unassigned/all");
        }

        public async Task<IEnumerable<IFilterView>> GetAllAssignedApplications()
        {
            return await Client.GetAsync<List<FilterView>>("/mine");
        }

        public async Task<IEnumerable<IFilterView>> GetAssignedByStatus(IEnumerable<string> statuses)
        {
            var uri = "/assigned/status/";
            uri = uri + string.Join("/", statuses);
            return await Client.GetAsync<List<FilterView>>(uri);
        }

        public async Task<IEnumerable<IFilterView>> GetUserWiseActiveLoans()
        {
             return await Client.GetAsync<List<FilterView>>("active-loans/mine");
        }

        public async Task<IEnumerable<IFilterView>> GetUserWiseApplicationReviewLoans()
        {
             return await Client.GetAsync<List<FilterView>>("application-review-loans/mine");
        }

        public async Task<IEnumerable<IFilterView>> GetUserWiseClosedLoans()
        {
            return await Client.GetAsync<List<FilterView>>("closed-loans/mine");
        }

        public async Task<Dictionary<string, LoanTypeCountAndAmount>> GetAmountAndCountByLoanTypeForCurrentMonth()
        {
            return await Client.GetAsync<Dictionary<string, LoanTypeCountAndAmount>>("metrics/total-count-loan-type/month");
        }

        public async Task<IEnumerable<IFilterView>> GetAllSuspendedLoans()
        {
            return await Client.GetAsync<List<FilterView>>("suspended-loans/all");
        }

        public async Task<IEnumerable<IFilterView>> GetAllActiveSuspendedLoans()
        {
            return await Client.GetAsync<List<FilterView>>("active-suspended-loans/all");
        }

        public async Task<IEnumerable<IFilterView>> GetUserWiseSuspendedLoans()
        {
             return await Client.GetAsync<List<FilterView>>("suspended-loans/mine");
        }

        public async Task<IEnumerable<IFilterView>> GetUserWiseActiveSuspendedLoans()
        {
             return await Client.GetAsync<List<FilterView>>("active-suspended-loans/mine");
        }

        public async Task<IEnumerable<IFilterView>> GetActiveOrClosedLoansByEmail(string email)
        {
            return await Client.GetAsync<List<FilterView>>("active-closed-loans/email/" + email);
        }
    }
}