﻿using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using System;
using LendFoundry.Foundation.Logging;
using Microsoft.Extensions.DependencyInjection;

namespace Docitt.Applications.Filters.Client
{
    public class ApplicationFilterClientFactory : IApplicationFilterClientFactory
    {

        [Obsolete("Need to use the overloaded with Uri")]
        public ApplicationFilterClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Uri = new UriBuilder("http", endpoint, port).Uri;
        }

        public ApplicationFilterClientFactory(IServiceProvider provider, Uri uri = null)
        {
            Provider = provider;
            Uri = uri;
        }
        private IServiceProvider Provider { get; set; }

        private Uri Uri { get; }


        public IApplicationFilterService Create(ITokenReader reader)
        {
            var uri = Uri;
            if (uri == null)
            {
                var logger = Provider.GetService<ILoggerFactory>().Create(NullLogContext.Instance);
                uri = Provider.GetRequiredService<IDependencyServiceUriResolverFactory>().Create(reader, logger).Get("application_filters");
            }


            var client = Provider.GetServiceClient(reader, uri);
            return new ApplicationFilterClient(client);
        }


        
    }
}