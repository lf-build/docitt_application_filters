﻿using LendFoundry.Security.Tokens;

namespace Docitt.Applications.Filters.Client
{
    public interface IApplicationFilterClientFactory
    {
        IApplicationFilterService Create(ITokenReader reader);
    }
}