﻿namespace Docitt.Applications.Filters
{
    public static class LoanType
    {
        public const string Purchase = "Purchase";
        public const string Refinance = "Refinance";
    }
}