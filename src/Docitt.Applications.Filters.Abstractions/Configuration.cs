﻿using System.Collections.Generic;
using LendFoundry.Foundation.Client;

namespace Docitt.Applications.Filters
{
    public class Configuration : IDependencyConfiguration
    {
        public EventMapping[] Events { get; set; }
        public string[] StatusesActiveLoans { get; set; }
         public string[] StatusesApplicationReviewLoans { get; set; }
        public string[] StatusesClosedLoans { get; set; }
        public string[] ApprovedStatuses { get; set; }
        public string[] SubmittedStatuses { get; set; }
        public string[] StatusesNotExpired { get; set; }
        public Dictionary<string, string> Dependencies { get; set; }
        public string Database { get; set; }

        public string ConnectionString { get; set; }
        public string[] StatusesSuspendedLoans { get; set; }
    }
}