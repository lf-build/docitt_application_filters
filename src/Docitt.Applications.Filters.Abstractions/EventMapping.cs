﻿namespace Docitt.Applications.Filters
{
    public class EventMapping
    {
        public string Name { get; set; }
        public string ApplicationNumber { get; set; }
    }
}