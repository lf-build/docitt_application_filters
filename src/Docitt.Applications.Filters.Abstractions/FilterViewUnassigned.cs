﻿using System;
using System.Linq;

namespace Docitt.Applications.Filters
{
    public class FilterViewUnassigned : IFilterViewUnassigned
    {
        public FilterViewUnassigned(IFilterView filterView)
        {
            ApplicationNumber = filterView.ApplicationId;
            if (filterView.Applicants != null || filterView.Applicants.Any())
            {
                FullName = filterView.Applicants[0].Name;
                Last4Ssn = GetLast4Digits(filterView.Applicants[0].Ssn);
            }
            ApplicationDate = filterView.Submitted;

        }

        private string GetLast4Digits(string ssn)
        {
            if (string.IsNullOrWhiteSpace(ssn))
                return string.Empty;

            if (ssn.Length <= 4)
                return ssn;

            return ssn.Substring(ssn.Length - 4);
        }

        public string ApplicationNumber { get; set; }

        public string FullName { get; set; }

        public string Last4Ssn { get; set; }

        public DateTimeOffset ApplicationDate { get; set; }
    }
}
