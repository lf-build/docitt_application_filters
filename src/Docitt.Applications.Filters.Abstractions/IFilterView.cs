﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using Docitt.Application;

namespace Docitt.Applications.Filters
{
    public interface IFilterView : IAggregate
    {
        string ApplicationId { get; set; }
        List<IApplicantFilterView> Applicants { get; set; }

        double DaysToClose { get; set; }
        DateTimeOffset Submitted { get; set; }
        double Amount { get; set; }

        string SourceType { get; set; }
        string SourceId { get; set; }

        DateTimeOffset ExpirationDate { get; set; }
        Status Status { get; set; }
        List<Status> CompletedStatuses { get; set; }

        TimeBucket ActiveOn { get; set; }

        string LoanType { get; set; }
        int TotalUnReadReminders { get; set; }

        Dictionary<string, string> Assignees { get; set; }


        double PropertyValue { get; set; }
        IAddress PropertyAddress { get; set; }
    }
}