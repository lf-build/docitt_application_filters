﻿using System;

namespace Docitt.Applications.Filters
{
  
    public class LoanTypeCountAndAmount : ILoanTypeCountAndAmount
    {
        public double TotalAmount { get; set; }
        public int TotalCount { get; set; }
    }
}
