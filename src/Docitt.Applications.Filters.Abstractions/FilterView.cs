﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using Docitt.Application;
using LendFoundry.Foundation.Client;
namespace Docitt.Applications.Filters
{
    public class FilterView : Aggregate, IFilterView
    {
        public string ApplicationId { get; set; }
        [JsonConverter(typeof(InterfaceListConverter<IApplicantFilterView, ApplicantFilterView>))]
        public List<IApplicantFilterView> Applicants { get; set; }

        public double DaysToClose { get; set; }
        public DateTimeOffset Submitted { get; set; }
        public double Amount { get; set; }

        public string SourceType { get; set; }
        public string SourceId { get; set; }

        public DateTimeOffset ExpirationDate { get; set; }
        public Status Status { get; set; }
        public List<Status> CompletedStatuses { get; set; }
        public TimeBucket ActiveOn { get; set; }
        public string LoanType { get; set; }

        public int TotalUnReadReminders { get; set; }

        public Dictionary<string, string> Assignees { get; set; }

        public double PropertyValue { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress PropertyAddress { get; set; }
    }
}