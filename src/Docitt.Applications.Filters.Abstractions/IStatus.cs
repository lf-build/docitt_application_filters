﻿namespace Docitt.Applications.Filters
{
    public interface IStatus
    {
        string Code { get; set; }
        string Name { get; set; }
        string Label { get; set; }
    }
}