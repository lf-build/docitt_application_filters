﻿namespace Docitt.Applications.Filters
{
    public class Status : IStatus
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Label { get; set; }
    }
}