using Docitt.Application;

namespace Docitt.Applications.Filters
{
    public class ApplicantFilterView : IApplicantFilterView
    {
        public string ApplicantId { get; set; }
        public string FirstName { get; set; }

        public string LastName { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Extension { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Country { get; set; }

        public string Ssn { get; set; }
        public bool IsPrimary { get; set; }
        public string UserName { get; set; }
        public ApplicantType ApplicantType{ get; set;}
    }
}