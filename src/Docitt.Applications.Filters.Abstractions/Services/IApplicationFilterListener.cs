﻿namespace Docitt.Applications.Filters
{
    public interface IApplicationFilterListener
    {
        void Start();
    }
}