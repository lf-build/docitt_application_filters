﻿using LendFoundry.Security.Tokens;

namespace Docitt.Applications.Filters
{
    public interface IFilterViewRepositoryFactory
    {
        IFilterViewRepository Create(ITokenReader reader);
    }
}