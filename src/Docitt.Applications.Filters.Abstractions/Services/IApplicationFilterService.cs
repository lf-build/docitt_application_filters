﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.Applications.Filters
{
    public interface IApplicationFilterService
    {
        Task<IEnumerable<IFilterView>> GetAllActiveLoans();

        /// <summary>
        /// Get the list of applications in REVIEW(200.10) state for application tab
        /// </summary>
        /// <returns>filer view for application pipeline</returns>
        Task<IEnumerable<IFilterView>> GetAllApplicationReviewLoans();

        Task<IEnumerable<IFilterView>> GetAllClosedLoans();

        IEnumerable<IFilterView> GetAllBySource(string sourceType, string sourceId);

        Task<IEnumerable<IFilterView>> GetByStatus(string sourceType, string sourceId, IEnumerable<string> statuses);

        Task<IEnumerable<IFilterView>> GetAll();

        Task<IEnumerable<IFilterView>> GetByStatus(IEnumerable<string> statuses);

        Task<double> GetTotalVolumeForCurrentMonthAndStatus(string sourceType, string sourceId);

        Task<double> GetAveragePerUnitForCurrentMonth(string sourceType, string sourceId);

        Task<Dictionary<string, double>> GetPercentageLoanTypeForCurrentMonth(string sourceType, string sourceId);

        Task<IFilterView> GetBySourceAndTypeWithApplicationNumber(string sourceType, string sourceId, string applicationNumber);

        Task<IFilterView> GetByApplicationNumber(string sourceType,  string applicationNumber);

        Task<IEnumerable<IFilterViewUnassigned>> GetAllUnassignedApplications();

        Task<IEnumerable<IFilterView>> GetAllAssignedApplications();

        Task<IEnumerable<IFilterView>> GetAssignedByStatus(IEnumerable<string> statuses);

        Task<IEnumerable<IFilterView>> GetUserWiseActiveLoans();

        /// <summary>
        /// Get the list applications in REVIEW(200.10) state for mine 
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<IFilterView>> GetUserWiseApplicationReviewLoans();
        Task<IEnumerable<IFilterView>> GetUserWiseClosedLoans();
        Task<Dictionary<string, LoanTypeCountAndAmount>> GetAmountAndCountByLoanTypeForCurrentMonth();
        
        Task<IEnumerable<IFilterView>> GetAllSuspendedLoans();

        Task<IEnumerable<IFilterView>> GetAllActiveSuspendedLoans();

        Task<IEnumerable<IFilterView>> GetUserWiseSuspendedLoans();

        Task<IEnumerable<IFilterView>> GetUserWiseActiveSuspendedLoans();
        Task<IEnumerable<IFilterView>> GetActiveOrClosedLoansByEmail(string email);
    }
}