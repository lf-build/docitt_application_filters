﻿using LendFoundry.Foundation.Persistence;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Docitt.Applications.Filters
{
    public interface IFilterViewRepository : IRepository<IFilterView>
    {
        void AddOrUpdate(IFilterView view);

        Task<IEnumerable<IFilterView>> GetAllActiveLoans(DateTimeOffset todayDate, string[] statusesActiveLoans);

        /// <summary>
        /// Get the list of all submitted applications in REVIEW(200.10) state
        /// </summary>
        /// <param name="todayDate">today's datetimeoffset</param>
        /// <param name="statusList">status list fecthed from configuration</param>
        /// <returns></returns>
        Task<IEnumerable<IFilterView>> GetAllApplicationReviewLoans(DateTimeOffset todayDate, string[] statusList);

        Task<IEnumerable<IFilterView>> GetAllClosedLoans(DateTimeOffset todayDate, string[] statusesClosedLoans);

        IEnumerable<IFilterView> GetAllBySource(string sourceType, string sourceId);

        Task<IEnumerable<IFilterView>> GetByStatus(string sourceType, string sourceId, IEnumerable<string> statuses);

        Task<IEnumerable<IFilterView>> GetByStatus(IEnumerable<string> statuses);

        Task<IEnumerable<IFilterView>> GetAll();

        Task<double> GetTotalVolumeForCurrentMonthAndStatus(string sourceType, string sourceId,
            DateTimeOffset currentMonth, IEnumerable<string> validStatuses);

        Task<double> GetAveragePerUnitForCurrentMonth(string sourceType, string sourceId, DateTimeOffset currentMonth,
            IEnumerable<string> validStatuses);

        Task<Dictionary<string, double>> GetPercentageLoanTypeForCurrentMonth(string sourceType, string sourceId,
            DateTimeOffset currentMonth, IEnumerable<string> validStatuses);
        Task<IFilterView> GetBySourceAndTypeWithApplicationNumber(string sourceType, string sourceId, string applicationNumber);
        Task<IFilterView> GetByApplicationNumber(string sourceType,  string applicationNumber);

        Task<IEnumerable<IFilterViewUnassigned>> GetAllUnassigned(IEnumerable<string> roles);

        Task<IEnumerable<IFilterView>> GetAllAssignedApplications(string userName);

        Task<IEnumerable<IFilterView>> GetAssignedByStatus(string userName, IList<string> statuses);

        Task<IEnumerable<IFilterView>> GetUserWiseActiveLoans(string userName, string[] statusesActiveLoans);
        
        /// <summary>
        /// Get the list of all submitted applications in REVIEW(200.10) state for mine
        /// </summary>
        /// <param name="userName">loanofficer username</param>
        /// <param name="statusList">status list fetched from configuration</param>
        /// <returns>returns application filterview</returns>
        Task<IEnumerable<IFilterView>> GetUserWiseApplicationReviewLoans(string userName, string[] statusList);

        Task<IEnumerable<IFilterView>> GetUserWiseClosedLoans(string userName, string[] statusesClosedLoans);

        Task<Dictionary<string, LoanTypeCountAndAmount>> GetTotalVolumeAndLoanCountByLoanTypeAndStatus(string userName, DateTimeOffset startDate, DateTimeOffset endDate, IEnumerable<string> validStatuses);

        Task<IFilterView> GetByApplicationNumber(string applicationNumber);        

        Task<IEnumerable<IFilterView>> GetAllSuspendedLoans(string[] statusesSuspendedLoans);

        Task<IEnumerable<IFilterView>> GetAllActiveSuspendedLoans(DateTimeOffset todayDate, string[] statusesActiveLoans, string[] statusesSuspendedLoans);

        Task<IEnumerable<IFilterView>> GetUserWiseSuspendedLoans(string userName, string[] statusesSuspendedLoans);

        Task<IEnumerable<IFilterView>> GetUserWiseActiveSuspendedLoans(string userName, string[] statusesActiveLoans, string[] statusesSuspendedLoans);

        Task<IEnumerable<IFilterView>> GetActiveOrClosedLoansByEmail(string email, string[] statusesActiveLoans,
            string[] statusesSuspendedLoans);
    }
}