using Docitt.Application;

namespace Docitt.Applications.Filters
{
    public interface IApplicantFilterView
    {
        string ApplicantId { get; set; }
        string FirstName { get; set; }

        string LastName { get; set; }
        string Name { get; set; }
        string Email { get; set; }
        string Address { get; set; }
        string City { get; set; }
        string State { get; set; }
        string ZipCode { get; set; }
        string Country { get; set; }
        string PhoneNumber { get; set; }
        string Extension { get; set; }

        string Ssn { get; set; }
        bool IsPrimary { get; set; }
        ApplicantType ApplicantType{ get; set;}
    }
}