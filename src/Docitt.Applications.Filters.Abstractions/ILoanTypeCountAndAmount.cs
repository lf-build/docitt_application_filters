﻿namespace Docitt.Applications.Filters
{
    public interface ILoanTypeCountAndAmount
    {
        double TotalAmount { get; set; }
        int TotalCount { get; set; }
    }
}