﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Docitt.Applications.Filters
{
    public interface IFilterViewUnassigned
    {
        string ApplicationNumber { get; set; }

        string FullName { get; set; }

        string Last4Ssn { get; set; }

        DateTimeOffset ApplicationDate { get; set; }
    }
}
