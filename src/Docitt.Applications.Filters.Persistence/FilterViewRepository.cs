﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Options;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Docitt.Application;

namespace Docitt.Applications.Filters.Persistence
{
    public class FilterViewRepository : MongoRepository<IFilterView, FilterView>, IFilterViewRepository
    {
        static FilterViewRepository()
        {

            BsonClassMap.RegisterClassMap<FilterView>(map =>
            {
                map.AutoMap();
                map.MapMember(m => m.Submitted).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                map.MapMember(m => m.ExpirationDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                map.MapMember(m => m.Assignees)
               .SetSerializer(new DictionaryInterfaceImplementerSerializer<Dictionary<string, string>>
                   (
                       DictionaryRepresentation.ArrayOfDocuments
                   )
               );
                var type = typeof(FilterView);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
            BsonClassMap.RegisterClassMap<ApplicantFilterView>(map =>
            {
                map.AutoMap();

                var applicantFilterViewType = typeof(ApplicantFilterView);
                map.SetDiscriminator(
                    $"{applicantFilterViewType.FullName}, {applicantFilterViewType.Assembly.GetName().Name}");
                map.MapProperty(p => p.ApplicantType).SetSerializer(new EnumSerializer<ApplicantType>(BsonType.String));
                map.SetIsRootClass(true);
            });
            BsonClassMap.RegisterClassMap<Status>(map =>
            {
                map.AutoMap();

                var applicantFilterViewStatus = typeof(Status);
                map.SetDiscriminator(
                    $"{applicantFilterViewStatus.FullName}, {applicantFilterViewStatus.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
            
            BsonClassMap.RegisterClassMap<Address>(map =>
            {
                map.AutoMap();
                map.MapProperty(p => p.Type).SetSerializer(new EnumSerializer<AddressType>(BsonType.String));

                var applicantFilterViewStatus = typeof(Address);
                map.SetDiscriminator(
                    $"{applicantFilterViewStatus.FullName}, {applicantFilterViewStatus.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public FilterViewRepository(ITenantService tenantService, IMongoConfiguration configuration)
            : base(tenantService, configuration, "application-filters")
        {
            CreateIndexIfNotExists("application", Builders<IFilterView>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.ApplicationId), true);

            CreateIndexIfNotExists("status-code", Builders<IFilterView>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.Status.Code));
            CreateIndexIfNotExists("status-name", Builders<IFilterView>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.Status.Name));
            CreateIndexIfNotExists("expiration-date", Builders<IFilterView>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.ExpirationDate));
            CreateIndexIfNotExists("source-type", Builders<IFilterView>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.SourceType));
            CreateIndexIfNotExists("source-id", Builders<IFilterView>.IndexKeys.Ascending(i => i.TenantId).Ascending(i => i.SourceId));
        }

        public void AddOrUpdate(IFilterView view)
        {
            if (view == null)
                throw new ArgumentNullException(nameof(view));

            System.Linq.Expressions.Expression<Func<IFilterView, bool>> query = l =>
                l.TenantId == TenantService.Current.Id &&
                l.ApplicationId == view.ApplicationId;

            view.TenantId = TenantService.Current.Id;

            try
            {
                Collection.FindOneAndReplace(query, view, new FindOneAndReplaceOptions<IFilterView, IFilterView> { IsUpsert = true });
            }
            catch (MongoWriteException)
            {
                //Retry one more time, if fail it will throw excepion
                Collection.FindOneAndReplace(query, view, new FindOneAndReplaceOptions<IFilterView, IFilterView> { IsUpsert = true });
            }
            catch (MongoCommandException)
            {
                //Retry one more time, if fail it will throw excepion
                Collection.FindOneAndReplace(query, view, new FindOneAndReplaceOptions<IFilterView, IFilterView> { IsUpsert = true });
            }
        }
        public async Task<IFilterView> GetBySourceAndTypeWithApplicationNumber(string sourceType, string sourceId, string applicationNumber)
        {
            return await Task.FromResult
                (
                    Query.FirstOrDefault(a =>
                        a.SourceType.ToLower() == sourceType.ToLower() &&
                        a.SourceId == sourceId &&
                        a.ApplicationId == applicationNumber)
                );
        }
        public async Task<IFilterView> GetByApplicationNumber( string applicationNumber)
        {
            return await Task.FromResult
                (
                    Query.FirstOrDefault(a =>         
                        a.ApplicationId == applicationNumber)
                );
        }
        public async Task<IFilterView> GetByApplicationNumber(string sourceType, string applicationNumber)
        {
            return await  Task.FromResult
                (
                    Query.FirstOrDefault(a =>
                        a.SourceType.ToLower() == sourceType.ToLower() &&
                      
                        a.ApplicationId == applicationNumber)
                );
        }

        public async Task<IEnumerable<IFilterView>> GetAllActiveLoans(DateTimeOffset todayDate, string[] statusesActiveLoans)
        {
            return await Task.FromResult
            (
                Query.Where(p => p.Status != null && statusesActiveLoans.Contains(p.Status.Code)).ToList()
                //Query.Where(p => p.ExpirationDate >= todayDate && p.Status != null && statusesActiveLoans.Contains(p.Status.Code)).ToList()

            );
        }

        public async Task<IEnumerable<IFilterView>> GetAllApplicationReviewLoans(DateTimeOffset todayDate, string[] statusesActiveLoans)
        {
            return await Task.FromResult
            (
                Query.Where(p => p.Status != null && statusesActiveLoans.Contains(p.Status.Code)).ToList()
            );
        }
       
        public async Task<IEnumerable<IFilterView>> GetAllClosedLoans(DateTimeOffset todayDate, string[] statusesClosedLoans)
        {
            return await Task.FromResult
            (

                Query.Where(p => p.Status != null && statusesClosedLoans.Contains(p.Status.Code)).ToList()

            );
        }

        public Task<IEnumerable<IFilterView>> GetAll()
        {
            return Task.FromResult<IEnumerable<IFilterView>>(OrderIt(Query));
        }

        public Task<IEnumerable<IFilterView>> GetByStatus(IEnumerable<string> statuses)
        {
            return Task.FromResult<IEnumerable<IFilterView>>(OrderIt(Query.Where(x => x.Status != null && statuses.Contains(x.Status.Code))));
        }

        public IEnumerable<IFilterView> GetAllBySource(string sourceType, string sourceId)
        {
            return OrderIt(Query.Where(x => x.SourceType.ToLower() == sourceType.ToLower() && x.SourceId == sourceId));
        }

/*
        public IEnumerable<IFilterView> GetBySourceAndStatus(string sourceType, string sourceId, IEnumerable<string> statuses)
        {
            return OrderIt(Query.Where(x => x.SourceType.ToLower() == sourceType.ToLower() && x.SourceId == sourceId && x.Status != null && statuses.Contains(x.Status.Code)));
        }
*/

        private IQueryable<IFilterView> OrderIt(IEnumerable<IFilterView> filterView)
        {
            return filterView.OrderBy(x => x.Status.Code)
                             .ThenBy(x => x.Submitted).AsQueryable();
        }

        public Task<IEnumerable<IFilterView>> GetByStatus(string sourceType, string sourceId, IEnumerable<string> statuses)
        {
            return Task.FromResult<IEnumerable<IFilterView>>
            (
                OrderIt(Query.Where(x => x.SourceType.ToLower() == sourceType.ToLower() &&
                                         x.SourceId == sourceId && x.Status != null &&
                                         statuses.Contains(x.Status.Code)))
            );
        }

        public async Task<double> GetTotalVolumeForCurrentMonthAndStatus(string sourceType, string sourceId, DateTimeOffset currentMonth, IEnumerable<string> validStatuses)
        {
            return await Task.FromResult(Query.Where
            (
                app => app.SourceType.ToLower() == sourceType.ToLower() &&
                       app.SourceId == sourceId &&
                       app.Submitted >= currentMonth && app.Status != null &&
                       validStatuses.Contains(app.Status.Code)
            ).Sum(x => x.Amount));
        }

        public async Task<double> GetAveragePerUnitForCurrentMonth(string sourceType, string sourceId, DateTimeOffset currentMonth, IEnumerable<string> validStatuses)
        {
            var currentMonthApplications = await Task.FromResult(Query.Where
             (
                 app => app.SourceType.ToLower() == sourceType.ToLower() &&
                        app.SourceId == sourceId &&
                        app.Submitted >= currentMonth && app.Status != null &&
                        validStatuses.Contains(app.Status.Code)
             ));
            var totalApplications = currentMonthApplications.Count();
            var totalAmounts = currentMonthApplications.Sum(x => x.Amount);
            if (totalApplications == 0)
                return 0;
            return totalAmounts / totalApplications;
        }

        public async Task<Dictionary<string, double>> GetPercentageLoanTypeForCurrentMonth(string sourceType, string sourceId, DateTimeOffset currentMonth, IEnumerable<string> validStatuses)
        {
            var currentMonthApplications = await Task.FromResult(Query.Where
            (
                app => app.SourceType.ToLower() == sourceType.ToLower() &&
                       app.SourceId == sourceId &&
                       app.Submitted >= currentMonth && app.Status != null &&
                       validStatuses.Contains(app.Status.Code)

            ));

            double totalPurchaseApplications = currentMonthApplications.Count(p => p.LoanType.ToLower() == LoanType.Purchase.ToLower());
            double totalRefinanceApplications = currentMonthApplications.Count(p => p.LoanType.ToLower() == LoanType.Refinance.ToLower());
           var totalLoanApplications= totalPurchaseApplications + totalRefinanceApplications;
            return new Dictionary<string, double>
            {
                { LoanType.Purchase, Math.Abs(totalLoanApplications )<=0?0 : Math.Ceiling( totalPurchaseApplications/(totalLoanApplications) * 100)},
                { LoanType.Refinance ,Math.Abs(totalLoanApplications )<=0?0 :Math.Floor(  totalRefinanceApplications/(totalLoanApplications) * 100)}
            };
        }

        public async Task<IEnumerable<IFilterViewUnassigned>> GetAllUnassigned(IEnumerable<string> roles)
        {
            var tenantId = TenantService.Current.Id;
            var builders = Builders<IFilterView>.Filter;
            var filterByTenantId = builders.Eq(db => db.TenantId, tenantId);
            var filterNotInRoles = builders.AnyNin("Assignees.k", roles);
            var result = await Collection.Find(builders.And(filterByTenantId, filterNotInRoles))
                .SortByDescending(a => a.ApplicationId)
                .ToListAsync();

            return result.Select(i => new FilterViewUnassigned(i)).AsEnumerable<IFilterViewUnassigned>().ToList();
        }

        public async Task<IEnumerable<IFilterView>> GetAllAssignedApplications(string userName)
        {
            var tenantId = TenantService.Current.Id;
            var builders = Builders<IFilterView>.Filter;
            var filterByTenantId = builders.Eq(db => db.TenantId, tenantId);
            var filterByUserName = builders.AnyIn("Assignees.k", new List<string>() { userName });
            return await Collection.Find(builders.And(filterByTenantId, filterByUserName))
                .SortByDescending(a => a.ApplicationId)
                .ToListAsync();
        }

        public async Task<IEnumerable<IFilterView>> GetAssignedByStatus(string userName, IList<string> statuses)
        {
            var tenantId = TenantService.Current.Id;
            var builders = Builders<IFilterView>.Filter;
            var filterByTenantId = builders.Eq(db => db.TenantId, tenantId);
            var filterByUserName = builders.AnyIn("Assignees.k", new List<string>() { userName });
            var filterByStatus = builders.Where(db => statuses.Contains(db.Status.Code));
            return await Collection.Find(builders.And(filterByTenantId, filterByUserName, filterByStatus))
                .SortByDescending(a => a.ApplicationId)
                .ToListAsync();
        }

        public async Task<IEnumerable<IFilterView>> GetUserWiseActiveLoans(string userName,string[] statusesActiveLoans)
        {
            var tenantId = TenantService.Current.Id;
            var builders = Builders<IFilterView>.Filter;
            var filterByTenantId = builders.Eq(db => db.TenantId, tenantId);
            var filterByUserName = builders.AnyIn("Assignees.k", new List<string>() { userName });
            var filterByStatus = builders.Where(db => statusesActiveLoans.Contains(db.Status.Code));
            return await Collection.Find(builders.And(filterByTenantId, filterByUserName, filterByStatus))
                .SortByDescending(a => a.ApplicationId)
                .ToListAsync();
        }

        public async Task<IEnumerable<IFilterView>> GetUserWiseApplicationReviewLoans(string userName,string[] statusList)
        {
            var tenantId = TenantService.Current.Id;
            var builders = Builders<IFilterView>.Filter;
            var filterByTenantId = builders.Eq(db => db.TenantId, tenantId);
            var filterByUserName = builders.AnyIn("Assignees.k", new List<string>() { userName });
            var filterByStatus = builders.Where(db => statusList.Contains(db.Status.Code));
            return await Collection.Find(builders.And(filterByTenantId, filterByUserName, filterByStatus))
                .SortByDescending(a => a.ApplicationId)
                .ToListAsync();
        }

        public async Task<IEnumerable<IFilterView>> GetUserWiseClosedLoans(string userName, string[] statusesClosedLoans)
        {
            var tenantId = TenantService.Current.Id;
            var builders = Builders<IFilterView>.Filter;
            var filterByTenantId = builders.Eq(db => db.TenantId, tenantId);
            var filterByUserName = builders.AnyIn("Assignees.k", new List<string>() { userName });
            var filterByStatus = builders.Where(db => statusesClosedLoans.Contains(db.Status.Code));
            return await Collection.Find(builders.And(filterByTenantId, filterByUserName, filterByStatus))
                .SortByDescending(a => a.ApplicationId)
                .ToListAsync();
        }


        public async Task<Dictionary<string, LoanTypeCountAndAmount>> GetTotalVolumeAndLoanCountByLoanTypeAndStatus(string userName, DateTimeOffset startDate, DateTimeOffset endDate, IEnumerable<string> validStatuses)
        {
            var tenantId = TenantService.Current.Id;
            var builders = Builders<IFilterView>.Filter;
            var filterByTenantId = builders.Eq(db => db.TenantId, tenantId);
            var filterByUserName = builders.AnyIn("Assignees.k", new List<string>() { userName });
            var filterByStartDate = builders.Gte(db => db.Submitted, startDate);
            var filterByEndDate = builders.Lte(db => db.Submitted, endDate);
            var filterByStatus = builders.Where(db => validStatuses.Contains(db.Status.Code));
            var filterByPurchaseType = builders.Eq(db => db.LoanType, LoanType.Purchase);
            var filterByRefinanceType = builders.Eq(db => db.LoanType, LoanType.Refinance);

            /* Purchase amount and no of closed count */
            var purchaseList = await Collection.Find(builders.And(filterByTenantId, filterByUserName, filterByStartDate, filterByEndDate, filterByPurchaseType, filterByStatus)).ToListAsync();

            var purchaseAmount = purchaseList.Sum(x => x.PropertyValue);
            var purchaseTotalCount = purchaseList.Count;
            var purchaseCountAndAmount = new LoanTypeCountAndAmount
            {
                TotalAmount = purchaseAmount,
                TotalCount = purchaseTotalCount
            };

            var purchaseRefinanceAmount =
                new Dictionary<string, LoanTypeCountAndAmount> {{LoanType.Purchase, purchaseCountAndAmount}};

            /* Refinance amount and no of closed count  */
            var refinanceList = await Collection.Find(builders.And(filterByTenantId, filterByUserName, filterByStartDate, filterByEndDate, filterByRefinanceType, filterByStatus)).ToListAsync();
            var refinanceAmount = refinanceList.Sum(x => x.PropertyValue);
            var refinanceTotalCount = refinanceList.Count;
            var refinanceCountAndAmount = new LoanTypeCountAndAmount
            {
                TotalAmount = refinanceAmount,
                TotalCount = refinanceTotalCount
            };

            purchaseRefinanceAmount.Add(LoanType.Refinance, refinanceCountAndAmount);
            
            return purchaseRefinanceAmount;
        }
        public async Task<IEnumerable<IFilterView>> GetAllSuspendedLoans(string[] statusesSuspendedLoans)
        {
            return await Task.FromResult
            (
                Query.Where(p => p.Status != null && statusesSuspendedLoans.Contains(p.Status.Code)).ToList()
            );
        }

        public async Task<IEnumerable<IFilterView>> GetAllActiveSuspendedLoans(DateTimeOffset todayDate, string[] statusesActiveLoans, string[] statusesSuspendedLoans)
        {
            return await Task.FromResult
            (
                Query.Where(p => (p.Status != null && statusesActiveLoans.Contains(p.Status.Code)) 
                            || (p.Status != null && statusesSuspendedLoans.Contains(p.Status.Code))).OrderBy(x => x.ApplicationId).ToList()
            );
        }

        public async Task<IEnumerable<IFilterView>> GetUserWiseSuspendedLoans(string userName, string[] statusesSuspendedLoans)
        {
            var tenantId = TenantService.Current.Id;
            var builders = Builders<IFilterView>.Filter;
            var filterByTenantId = builders.Eq(db => db.TenantId, tenantId);
            var filterByUserName = builders.AnyIn("Assignees.k", new List<string>() { userName });
            var filterByStatus = builders.Where(db => statusesSuspendedLoans.Contains(db.Status.Code));
            return await Collection.Find(builders.And(filterByTenantId, filterByUserName, filterByStatus))
                .SortByDescending(a => a.ApplicationId)
                .ToListAsync();
        }

        public async Task<IEnumerable<IFilterView>> GetUserWiseActiveSuspendedLoans(string userName, string[] statusesActiveLoans, string[] statusesSuspendedLoans)
        {
            var tenantId = TenantService.Current.Id;
            var builders = Builders<IFilterView>.Filter;
            var filterByTenantId = builders.Eq(db => db.TenantId, tenantId);
            var filterByUserName = builders.AnyIn("Assignees.k", new List<string>() { userName });
            var filterByStatus = builders.Where(db => (statusesActiveLoans.Contains(db.Status.Code) || statusesSuspendedLoans.Contains(db.Status.Code)));
            return await Collection.Find(builders.And(filterByTenantId, filterByUserName, filterByStatus))
                .SortByDescending(a => a.ApplicationId)
                .ToListAsync();
        }

        public async Task<IEnumerable<IFilterView>> GetActiveOrClosedLoansByEmail(string email,
            string[] statusesActiveLoans, string[] statusesClosedLoans)
        {
            var allStatus = statusesActiveLoans.Concat(statusesClosedLoans);
            var tenantId = TenantService.Current.Id;
            var builders = Builders<IFilterView>.Filter;
            var filterByTenantId = builders.Eq(db => db.TenantId, tenantId);
            var filterByUserName = builders.AnyIn("Applicants.Email", new List<string>() { email });
            var filterByStatus = builders.Where(db => (allStatus.Contains(db.Status.Code)));
            return await Collection.Find(builders.And(filterByTenantId, filterByUserName, filterByStatus))
                .SortByDescending(a => a.Submitted)               
                .ToListAsync();
        }
        
    }
}