﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Docitt.Applications.Filters.Persistence
{
    public class FilterViewRepositoryFactory : IFilterViewRepositoryFactory
    {
        public FilterViewRepositoryFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IFilterViewRepository Create(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var mongoConfigurationFactory = Provider.GetService<IMongoConfigurationFactory>();
            var mongoConfiguration = mongoConfigurationFactory.Get(reader, Provider.GetService<ILogger>());

          
            return new FilterViewRepository(tenantService, mongoConfiguration);
        }
    }
}